# --- !Ups

CREATE TABLE `bank_accounts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_verified` tinyint(1) DEFAULT '0',
  `account_number` varchar(32) DEFAULT NULL,
  `ifsc_code` varchar(16) DEFAULT NULL,
  `client_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

CREATE TABLE `client_configurations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `property_key` varchar(96) NOT NULL,
  `property_value` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `fb_access_tokens` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `access_token` varchar(255) DEFAULT NULL,
  `valid_upto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

CREATE TABLE `features` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `source` varchar(255) NOT NULL,
  `identity_engine` tinyint(1) DEFAULT '0',
  `intent_engine` tinyint(1) DEFAULT '0',
  `min_threshold` float DEFAULT NULL,
  `max_threshold` float DEFAULT NULL,
  `is_asc` tinyint(1) DEFAULT '0',
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `capacity_engine` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

CREATE TABLE `loans` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `applied_at` datetime NOT NULL,
  `amount` bigint(20) DEFAULT '0',
  `repaid_amount` bigint(20) DEFAULT '0',
  `interest_amount` bigint(20) DEFAULT '0',
  `outstanding_amount` bigint(20) DEFAULT '0',
  `emi_amount` int(11) DEFAULT NULL,
  `interest_rate` decimal(4,2) DEFAULT NULL,
  `purpose` varchar(96) DEFAULT NULL,
  `loan_type` varchar(16) DEFAULT NULL,
  `status` varchar(16) DEFAULT NULL,
  `emi_count` int(11) DEFAULT NULL,
  `emi_date` int(11) DEFAULT NULL,
  `client_id` bigint(20) DEFAULT NULL,
  `disbursed_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `payments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `amount` bigint(20) NOT NULL DEFAULT '0',
  `loan_id` bigint(20) NOT NULL,
  `status` varchar(32) NOT NULL,
  `mode` varchar(32) DEFAULT NULL,
  `ref_number` varchar(255) DEFAULT NULL,
  `from_bank_ac_number` varchar(255) DEFAULT NULL,
  `payment_deadline` datetime NOT NULL,
  `payment_date` datetime DEFAULT NULL,
  `client_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `profile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `client_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `pan` varchar(12) DEFAULT NULL,
  `aadhaar` varchar(12) DEFAULT NULL,
  `address` varchar(1048) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `phone_number` varchar(16) DEFAULT NULL,
  `is_verified` tinyint(1) DEFAULT '0',
  `registration_complete` tinyint(1) DEFAULT '0',
  `fb_id` varchar(255) DEFAULT NULL,
  `fb_name` varchar(255) DEFAULT NULL,
  `fb_first_name` varchar(255) DEFAULT NULL,
  `fb_last_name` varchar(255) DEFAULT NULL,
  `fb_min_age` int(11) DEFAULT NULL,
  `fb_max_age` int(11) DEFAULT NULL,
  `fb_gender` varchar(8) DEFAULT NULL,
  `fb_locale` varchar(64) DEFAULT NULL,
  `fb_time_zone` double DEFAULT NULL,
  `fb_updated_time` datetime DEFAULT NULL,
  `fb_user_friends` varchar(2096) DEFAULT NULL,
  `fb_email` varchar(255) DEFAULT NULL,
  `fb_dob` datetime DEFAULT NULL,
  `fb_verified` tinyint(1) DEFAULT '0',
  `fb_picture_uploaded` tinyint(1) DEFAULT '0',
  `fb_picture_url` varchar(255) DEFAULT NULL,
  `monthly_income` int(11) DEFAULT '0',
  `fb_access_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

CREATE TABLE `sms_patterns` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pattern` varchar(2048) DEFAULT NULL,
  `txn_type` varchar(7) DEFAULT NULL,
  `account_number_group_index` int(11) DEFAULT NULL,
  `amount_group_index` int(11) DEFAULT NULL,
  `is_verified` tinyint(1) DEFAULT '0',
  `is_algo_generated` tinyint(1) DEFAULT '0',
  `sender` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `merchant_name` varchar(255) DEFAULT NULL,
  `account_type` varchar(255) DEFAULT NULL,
  `merchant_account_number_group_index` int(11) DEFAULT NULL,
  `available_limit_group_index` int(11) DEFAULT NULL,
  `bill_date_group_index` int(11) DEFAULT NULL,
  `bill_month_group_index` int(11) DEFAULT NULL,
  `bill_due_date_group_index` int(11) DEFAULT NULL,
  `outstanding_amount_group_index` int(11) DEFAULT NULL,
  `total_amount_due_group_index` int(11) DEFAULT NULL,
  `credit_limit_group_index` int(11) DEFAULT NULL,
  `date_group_index` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1465912742021 DEFAULT CHARSET=latin1;

CREATE TABLE `user_capacity_scores` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `processing_version` bigint(20) DEFAULT NULL,
  `max_emi` int(11) DEFAULT NULL,
  `max_loan` int(11) DEFAULT NULL,
  `max_duration` int(11) DEFAULT NULL,
  `min_interest` float DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `user_credit_data` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `interest_rate` decimal(5,2) DEFAULT NULL,
  `min_interest_rate` double DEFAULT NULL,
  `other_charges` double DEFAULT NULL,
  `max_duration` int(11) DEFAULT NULL,
  `max_emi` double DEFAULT NULL,
  `loan_limit` int(11) DEFAULT NULL,
  `max_loan_limit` int(11) DEFAULT NULL,
  `min_duration` int(11) DEFAULT NULL,
  `min_amount` int(11) DEFAULT NULL,
  `default_duration` int(11) DEFAULT NULL,
  `default_amount` int(11) DEFAULT NULL,
  `emi_date` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `processing_version` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `user_data` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `data_type` varchar(255) DEFAULT NULL,
  `data` varchar(3000) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5973 DEFAULT CHARSET=latin1;

CREATE TABLE `user_devices` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `rooted` tinyint(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deactivated_at` datetime DEFAULT NULL,
  `utm_source` varchar(96) DEFAULT NULL,
  `utm_campaign` varchar(96) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

CREATE TABLE `user_features` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `feature` varchar(255) DEFAULT NULL,
  `value` float DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `processing_version` bigint(20) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

CREATE TABLE `user_identity_scores` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `processing_version` bigint(20) DEFAULT NULL,
  `verified` tinyint(1) DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `user_intent_scores` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `processing_version` bigint(20) DEFAULT NULL,
  `score` float DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `user_topic_updates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `topic` varchar(255) DEFAULT NULL,
  `processing_version` bigint(20) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `access_token` varchar(255) NOT NULL,
  `fcm_token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_sync_timestamp` bigint(20) DEFAULT '0',
  `fb_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fb_id` (`fb_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


# --- !Downs
DROP TABLE bank_accounts ;
DROP TABLE client_configurations ;
DROP TABLE fb_access_tokens ;
DROP TABLE features ;
DROP TABLE loans ;
DROP TABLE payments ;
DROP TABLE profile ;
DROP TABLE sms_patterns ;
DROP TABLE user_capacity_scores ;
DROP TABLE user_credit_data ;
DROP TABLE user_data ;
DROP TABLE user_devices ;
DROP TABLE user_features ;
DROP TABLE user_identity_scores ;
DROP TABLE user_intent_scores ;
DROP TABLE user_topic_updates ;
DROP TABLE users ;

