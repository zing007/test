# --- !Ups
ALTER TABLE user_topic_updates ADD COLUMN retry_count integer default 0;

create table cron (
  id                            bigint auto_increment not null,
  version                       bigint not null,
  topic                         varchar(255) not null,
  status                        varchar(10),
  producer_host                 varchar(255),
  consumer_host                 varchar(255),
  description                   varchar(255),
  created_at                    datetime(6) not null,
  updated_at                    datetime(6) not null,
  constraint ck_cron_status check (status in ('PENDING', 'PROCESSING', 'COMPLETED', 'FAILED')),
  constraint pk_user_file primary key (id)
);

ALTER TABLE lender ADD COLUMN email VARCHAR(32);

# --- !Downs
ALTER TABLE lender DROP COLUMN email;
ALTER TABLE user_topic_updates DROP COLUMN retry_count;
DROP TABLE cron;
