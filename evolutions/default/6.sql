# --- !Ups

create table user_derived_profile (
  id                            bigint auto_increment not null,
  user_id                       bigint,
  is_aadhaar_verified           tinyint(1) default 0,
  is_pan_verified               tinyint(1) default 0,
  is_cheque_verified            tinyint(1) default 0,
  is_valid_aadhar_image         tinyint(1) default 0,
  aadhaar_number                varchar(255),
  aadhaar_name                  varchar(255),
  aadhaar_dob                   datetime(6),
  aadhaar_gender                varchar(255),
  aadhaar_address               varchar(255),
  aadhaar_city                  varchar(255),
  aadhaar_pincode               varchar(255),
  is_valid_pan_image            tinyint(1) default 0,
  pan_number                    varchar(255),
  pan_name                      varchar(255),
  pan_father_name               varchar(255),
  pan_dob                       datetime(6),
  is_valid_cheque_image         tinyint(1) default 0,
  bank_account_name             varchar(255),
  bank_account_number           varchar(255),
  bank_name                     varchar(255),
  bank_address                  varchar(255),
  bank_city                     varchar(255),
  bank_pincode                  varchar(255),
  is_selfie_aadhaar_match       tinyint(1) default 0,
  is_selfie_pan_match           tinyint(1) default 0,
  is_pan_bank_sign_match        tinyint(1) default 0,
  aadhaar_image                 varchar(255),
  pan_image                     varchar(255),
  cheque_image                  varchar(255),
  selfie_image                  varchar(255),
  created_at                    datetime(6) not null,
  updated_at                    datetime(6) not null,
  aadhaar_aua_response          varchar(1024),
  constraint pk_user_derived_profile primary key (id)
);


# --- !Downs
DROP TABLE user_derived_profile;
