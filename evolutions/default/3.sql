# --- !Ups
ALTER TABLE features add column waiting_engine tinyint(1) DEFAULT '0';
ALTER TABLE features add column description varchar(255);
ALTER TABLE profile DROP COLUMN is_verified;
ALTER TABLE profile DROP COLUMN registration_complete;
ALTER TABLE profile ADD COLUMN kyc_status VARCHAR(255);
ALTER TABLE profile ADD COLUMN kyc_date DATETIME;

# --- !Downs
ALTER TABLE features drop column description;
ALTER TABLE features drop column waiting_engine;
ALTER TABLE profile ADD COLUMN is_verified tinyint(1);
ALTER TABLE profile ADD COLUMN registration_complete tinyint(1);
ALTER TABLE profile DROP COLUMN kyc_status;
ALTER TABLE profile DROP COLUMN kyc_date;
