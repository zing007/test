# --- !Ups
create index utu__uid_topic_pv  on user_topic_updates(user_id, topic(5), processing_version) using btree;
create index utu__deleted_pv_rc on user_topic_updates(deleted, processing_version, retry_count) using btree;
create table user_coordinates(
  id bigint auto_increment not null,
  user_id bigint(20) NOT NULL,
  lat float,
  lng float,
  location_origin_type varchar(16),
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  constraint pk_user_coordinates primary key (id)
);
ALTER TABLE user_coordinates ADD INDEX `user_id` (`user_id`);
ALTER TABLE user_distances add column real_lat float;
ALTER TABLE user_distances add column real_lng float;
ALTER TABLE user_derived_profile DROP COLUMN lat;
ALTER TABLE user_derived_profile DROP COLUMN lng;
ALTER TABLE user_derived_profile ADD COLUMN address varchar(511);
ALTER TABLE user_derived_profile ADD COLUMN city varchar(64);
ALTER TABLE user_derived_profile ADD COLUMN pincode varchar(6);
ALTER TABLE pin_code_locations ADD COLUMN city varchar(64);
ALTER TABLE pin_code_locations ADD INDEX `idx_pin_code` (`pin_code`);

# --- !Downs
drop index `utu__uid_topic_pv` on user_topic_updates;
drop index `utu__deleted_pv_rc` on user_topic_updates;
drop table user_coordinates;
ALTER TABLE user_distances DROP COLUMN real_lat;
ALTER TABLE user_distances DROP COLUMN real_lng;
ALTER TABLE user_derived_profile ADD COLUMN lat float;
ALTER TABLE user_derived_profile ADD COLUMN lng float;
ALTER TABLE pin_code_locations DROP INDEX `idx_pin_code`;
ALTER TABLE user_derived_profile DROP COLUMN address;
ALTER TABLE user_derived_profile DROP COLUMN city;
ALTER TABLE user_derived_profile DROP COLUMN pincode;
ALTER TABLE pin_code_locations DROP COLUMN city;