# --- !Ups
alter table  user_derived_profile change column is_aadhaar_verified  is_aadhaar_processed tinyint(1);
alter table  user_derived_profile  change column is_pan_verified is_pan_processed tinyint(1);
alter table  user_derived_profile change column is_cheque_verified is_cheque_processed tinyint(1);

create table user_processed_data (
  id                            bigint auto_increment not null,
  user_id                       bigint,
  data_type                     varchar(255),
  compressed_data               longblob,
  processing_version            bigint,
  created_at                    datetime(6) not null,
  updated_at                    datetime(6) not null,
  constraint ck_user_processed_data_data_type check (data_type in ('SMS','CALL_LOGS','LOCATION','BROWSING_HISTORY','CONTACTS','DEVICE','INSTALLED_APPS','REF_CONTACTS','UNKNOWN')),
  constraint pk_user_processed_data primary key (id)
);

create table cell_locations (
    id bigint(20) auto_increment not null,
    mobile_country_code int,
    mobile_network_code int,
    mobile_signal_strength int,
    cell_id int,
    location_area_code int,
    wifi_mac_id varchar(32),
    wifi_frequency int,
    wifi_signal_strength int,
    lat float,
    lng float,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    constraint pk_cell_location primary key (id)
);

CREATE INDEX mcc_mnc_cid_lac_index ON cell_locations (mobile_country_code, mobile_network_code, cell_id, location_area_code);
CREATE INDEX mac_index ON cell_locations (wifi_mac_id);

create table user_distances(
    id bigint(20) auto_increment NOT NULL,
    user_id bigint(20) NOT NULL,
    data_src varchar(16),
    origin varchar(16),
    distance FLOAT,
    recorded_at bigint(20),
    processing_version bigint(20),
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    constraint pk_user_distance primary key (id)
);
CREATE INDEX uid_org_dsrc_index ON user_distances (user_id, origin, data_src);

create table pin_code_locations(
    id bigint(20) auto_increment NOT NULL,
    pin_code int(8) UNIQUE NOT NULL,
    lat float,
    lng float,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    constraint pk_pin_code_location primary key (id)
);

ALTER TABLE user_derived_profile ADD COLUMN lat float;
ALTER TABLE user_derived_profile ADD COLUMN lng float;
ALTER TABLE user_derived_profile ADD COLUMN fb_friend_count int DEFAULT 0;


# --- !Downs
alter table  user_derived_profile change column is_aadhaar_processed  is_aadhaar_verified tinyint(1);
alter table  user_derived_profile  change column is_pan_processed is_pan_verified tinyint(1);
alter table  user_derived_profile change column is_cheque_processed is_cheque_verified tinyint(1);

drop table user_processed_data;

DROP TABLE cell_locations;
ALTER TABLE user_derived_profile DROP COLUMN lat;
ALTER TABLE user_derived_profile DROP COLUMN lng;
DROP TABLE user_distances;
DROP table pin_code_locations;
ALTER TABLE user_derived_profile DROP COLUMN fb_friend_count;
