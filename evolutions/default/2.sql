# --- !Ups
CREATE TABLE `otp_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `otp` varchar(10) NOT NULL,
  `phone_number` varchar(16) NOT NULL,
  `status` varchar(32) DEFAULT NULL,
  `expiry_date` timestamp NOT NULL,
  `request_id` varchar(64),
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE profile ADD COLUMN city VARCHAR(255);
ALTER TABLE profile ADD COLUMN pin_code VARCHAR(255);

create table user_transactions (
  id                            bigint auto_increment not null,
  user_id                       bigint,
  amount                        integer,
  status                        varchar(255),
  description                   varchar(255),
  created_at                    datetime(6) not null,
  updated_at                    datetime(6) not null,
  constraint ck_user_transactions_status check (status in ('PENDING','SUCCESS','FAILED','UNKNOWN')),
  constraint pk_user_transactions primary key (id)
);

create table atos_pg_response (
  id                            bigint auto_increment not null,
  oder_id                       varchar(255),
  txn_reference_number          varchar(255),
  amount                        varchar(255),
  txn_status                    varchar(255),
  description                   varchar(255),
  bank_reference_number         varchar(255),
  bank_auth_code                varchar(255),
  bank_response_code            varchar(255),
  txn_date_time                 varchar(255),
  created_at                    datetime(6) not null,
  updated_at                    datetime(6) not null,
  constraint pk_atos_pg_response primary key (id)
);

# --- !Downs
DROP TABLE otp_details;
ALTER TABLE profile DROP COLUMN city;
ALTER TABLE profile DROP COLUMN pin_code;

drop table user_transactions;
drop table atos_pg_response;

