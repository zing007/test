# --- !Ups
ALTER TABLE profile CHANGE company_name workplace_name VARCHAR(255);
ALTER TABLE profile ADD COLUMN workplace_address VARCHAR(255);
ALTER TABLE profile ADD COLUMN workplace_pincode VARCHAR(255);
# --- !Downs
ALTER TABLE profile CHANGE workplace_name company_name VARCHAR(255);
ALTER TABLE profile DROP COLUMN workplace_address;
ALTER TABLE profile DROP COLUMN workplace_pincode;