# --- !Ups
create table admin_overridden_limit (
  id                            bigint auto_increment not null,
  user_id                       bigint,
  min_amount                    integer,
  max_amount                    integer,
  description                   varchar(255),
  created_at                    datetime(6) not null,
  updated_at                    datetime(6) not null,
  constraint pk_admin_overridden_limit primary key (id),
  UNIQUE KEY `user_id` (`user_id`)
);

# --- !Downs
drop table admin_overridden_limit


