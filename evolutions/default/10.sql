# --- !Ups
ALTER TABLE profile ADD COLUMN workplace_designation VARCHAR(255);
# --- !Downs
ALTER TABLE profile DROP COLUMN workplace_designation;