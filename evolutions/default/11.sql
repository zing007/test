# --- !Ups
create table user_file (
  id                            bigint auto_increment not null,
  user_id                       bigint,
  file_name                     varchar(255),
  status                        varchar(13),
  message                       TEXT,
  created_at                    datetime(6) not null,
  updated_at                    datetime(6) not null,
  constraint ck_user_file_status check (status in ('FAILED','LOCAL_SUCCESS','SUCCESS')),
  constraint pk_user_file primary key (id)
);
# --- !Downs
DROP TABLE user_file;