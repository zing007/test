# --- !Ups
ALTER TABLE otp_details MODIFY expiry_date datetime NOT NULL;

# --- !Downs
ALTER TABLE otp_details MODIFY expiry_date timestamp NOT NULL;
