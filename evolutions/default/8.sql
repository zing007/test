# --- !Ups
create table loan_product (
  id                            bigint auto_increment not null,
  amount                        bigint not null,
  interest_rate                 double not null,
  other_charges                 integer not null,
  emi_count                     integer not null,
  duration                      integer not null,
  duration_unit                 varchar(5)  not null,
  start_date                    datetime(6) not null,
  end_date                      datetime(6) not null,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  constraint pk_loan_products primary key (id)
);

create table user_loan_product (
  id                            bigint auto_increment not null,
  user_id                       bigint,
  loan_product_id               bigint,
  status                        varchar(8),
  created_at                    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at                    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  constraint pk_user_loan_product primary key (id)
);

create table lender (
  id                            bigint auto_increment not null,
  name                          varchar(255) not null,
  ac_no                         varchar(32),
  ifsc                          varchar(16),
  bank_name                     varchar(64),
  vpa                           varchar(64),
  created_at                    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at                    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  constraint pk_lender primary key (id)
);

alter table loans drop column repaid_amount;
alter table loans drop column interest_amount;
alter table loans drop column outstanding_amount;
alter table loans drop column emi_amount;
alter table loans drop column interest_rate;
alter table loans drop column emi_count;
alter table loans add column loan_product_id bigint;
alter table loans add column lender_id bigint(20);
alter table loans RENAME user_loan;
alter table payments change loan_id user_loan_id bigint(20);

alter table user_identity_scores add column status varchar(7);
alter table user_identity_scores add constraint ck_user_identity_scores_status check (status in ('SUCCESS','FAILED','PENDING','UNKNOWN'));
alter table user_capacity_scores add column status varchar(7);
alter table user_capacity_scores add constraint ck_user_identity_scores_status check (status in ('SUCCESS','FAILED','PENDING','UNKNOWN'));
alter table user_intent_scores add column status varchar(7);
alter table user_intent_scores add constraint ck_user_identity_scores_status check (status in ('SUCCESS','FAILED','PENDING','UNKNOWN'));

alter table user_credit_data add column lender_id bigint(20);

alter table user_derived_profile add column is_pan_ao_checked tinyint(1) default 0;
alter table user_derived_profile add column bank_ifsc_code varchar(255);



# --- !Downs
drop table loan_product;
drop table user_loan_product;
alter table user_loan add column repaid_amount bigint;
alter table user_loan add column interest_amount bigint;
alter table user_loan add column outstanding_amount bigint;
alter table user_loan add column emi_amount integer;
alter table user_loan add column interest_rate decimal(4,2);
alter table user_loan add column emi_count integer;
alter table user_loan drop column lender_id;
alter table user_loan RENAME loans;
alter table loans drop column loan_product_id;
alter table payments change user_loan_id loan_id bigint(20);

alter table user_identity_scores drop column status ;
alter table user_identity_scores drop constraint ck_user_identity_scores_status ;
alter table user_capacity_scores drop column status ;
alter table user_capacity_scores drop constraint ck_user_identity_scores_status ;
alter table user_intent_scores drop column status ;
alter table user_intent_scores drop constraint ck_user_identity_scores_status ;

alter table user_credit_data drop column lender_id;

alter table user_derived_profile drop column is_pan_ao_checked ;
alter table user_derived_profile drop column bank_ifsc_code;

