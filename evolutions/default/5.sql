# --- !Ups
ALTER TABLE user_features ADD COLUMN source varchar(255);
ALTER TABLE user_topic_updates ADD COLUMN deleted tinyint(1) DEFAULT 0;

ALTER TABLE user_credit_data ADD INDEX `user_id` (`user_id`);
ALTER TABLE  bank_accounts ADD INDEX `user_id` (`user_id`);
ALTER TABLE  bank_accounts ADD INDEX `account_number` (`account_number`);
ALTER TABLE  fb_access_tokens ADD INDEX `user_id` (`user_id`);
ALTER TABLE  loans ADD INDEX `user_id` (`user_id`);
ALTER TABLE  loans ADD INDEX `status` (`status`);
ALTER TABLE  payments ADD INDEX `status` (`status`);
ALTER TABLE  payments ADD INDEX `user_id` (`user_id`);
ALTER TABLE  profile ADD INDEX `aadhaar` (`aadhaar`);
ALTER TABLE  profile ADD INDEX `pan` (`pan`);
ALTER TABLE  profile ADD INDEX `phone_number` (`phone_number`);
ALTER TABLE  user_data ADD INDEX `user_id` (`user_id`);
ALTER TABLE  otp_details ADD INDEX `user_id` (`user_id`);
ALTER TABLE  user_transactions ADD INDEX `user_id` (`user_id`);
ALTER TABLE  atos_pg_response ADD INDEX `oder_id` (`oder_id`);
# --- !Downs
ALTER TABLE user_features DROP COLUMN source;
ALTER TABLE user_topic_updates DROP COLUMN deleted;

ALTER TABLE user_credit_data DROP INDEX `user_id`;
ALTER TABLE  bank_accounts DROP INDEX `user_id`;
ALTER TABLE  bank_accounts DROP INDEX `account_number`;
ALTER TABLE  fb_access_tokens DROP INDEX `user_id`;
ALTER TABLE  loans DROP INDEX `user_id`;
ALTER TABLE  loans DROP INDEX `status`;
ALTER TABLE  payments DROP INDEX `status`;
ALTER TABLE  payments DROP INDEX `user_id`;
ALTER TABLE  profile DROP INDEX `aadhaar`;
ALTER TABLE  profile DROP INDEX `pan`;
ALTER TABLE  profile DROP INDEX `phone_number`;
ALTER TABLE  user_data DROP INDEX `user_id`;
ALTER TABLE  otp_details DROP INDEX `user_id`;
ALTER TABLE  user_transactions DROP INDEX `user_id`;
ALTER TABLE  atos_pg_response DROP INDEX `oder_id`;
