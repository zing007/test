# ---!Ups

insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name) values (40, 4000, 3, 200, 1, 1, 'MONTH', '2017-08-25',
       '2027-06-20', now(), now(), 'WAITVLP_4000_200_1MONTH');

insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name) values (41, 6000, 3, 300, 1, 1, 'MONTH', '2017-08-25',
       '2027-06-20', now(), now(), 'WAITVLP_6000_300_1MONTH');

insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name) values (42, 10000, 3, 200, 2, 2, 'MONTH', '2017-08-25',
       '2027-06-20', now(), now(), 'WAITVLP_10000_200_2MONTH');


insert into loan_product_dependency (id, rule, sub_rule, type, param1, param2, param3, param4, param5,
        created_at, updated_at, target, enabled, description) values
        (40, 'wait_vlp_4000', 'wait', 'MVEL_EXPRESSION', 'input.showWaitingVLP()',
        NULL, NULL, NULL, NULL, now(), now(), 'WAITVLP_4000_200_1MONTH', 1, 'Waiting VLP 4000  - wait');
insert into loan_product_dependency (id, rule, sub_rule, type, param1, param2, param3, param4, param5,
        created_at, updated_at, target, enabled, description) values
        (41, 'wait_vlp_4000', 'ladder', 'LOAN', '3000',
        '1', '1', 'MONTH', NULL, now(), now(), 'WAITVLP_4000_200_1MONTH', 1, 'Waiting VLP 4000  - wait');


insert into loan_product_dependency (id, rule, sub_rule, type, param1, param2, param3, param4, param5,
        created_at, updated_at, target, enabled, description) values
        (42, 'wait_vlp_6000', 'wait', 'MVEL_EXPRESSION', 'input.showWaitingVLP()',
        NULL, NULL, NULL, NULL, now(), now(), 'WAITVLP_6000_300_1MONTH', 1, 'Waiting VLP 6000  - wait');
insert into loan_product_dependency (id, rule, sub_rule, type, param1, param2, param3, param4, param5,
        created_at, updated_at, target, enabled, description) values
        (43, 'wait_vlp_6000', 'ladder', 'LOAN', '4000',
        '2', '1', 'MONTH', NULL, now(), now(), 'WAITVLP_6000_300_1MONTH', 1, 'Waiting VLP 6000  - wait');


insert into loan_product_dependency (id, rule, sub_rule, type, param1, param2, param3, param4, param5,
        created_at, updated_at, target, enabled, description) values
        (44, 'wait_vlp_10000', 'wait', 'MVEL_EXPRESSION', 'input.showWaitingVLP()',
        NULL, NULL, NULL, NULL, now(), now(), 'WAITVLP_10000_200_2MONTH', 1, 'Waiting VLP 10000  - wait');
insert into loan_product_dependency (id, rule, sub_rule, type, param1, param2, param3, param4, param5,
        created_at, updated_at, target, enabled, description) values
        (45, 'wait_vlp_10000', 'ladder', 'LOAN', '6000',
        '2', '1', 'MONTH', NULL, now(), now(), 'WAITVLP_10000_200_2MONTH', 1, 'Waiting VLP 10000  - wait');


insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (40009, 40, 9, '2017-08-25', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (40010, 40, 10, '2017-08-25', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (40011, 40, 11, '2017-08-25', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (40012, 40, 12, '2017-08-25', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (40013, 40, 13, '2017-08-25', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (40014, 40, 14, '2017-08-25', now() );

insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (41009, 41, 9, '2017-08-25', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (41010, 41, 10, '2017-08-25', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (41011, 41, 11, '2017-08-25', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (41012, 41, 12, '2017-08-25', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (41013, 41, 13, '2017-08-25', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (41014, 41, 14, '2017-08-25', now() );

insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (42009, 42, 9, '2017-08-25', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (42010, 42, 10, '2017-08-25', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (42011, 42, 11, '2017-08-25', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (42012, 42, 12, '2017-08-25', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (42013, 42, 13, '2017-08-25', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (42014, 42, 14, '2017-08-25', now() );


# ---!Downs

delete from loan_product where id in (40, 41, 42);
delete from loan_product_dependency where id in (40, 41, 42, 43, 44, 45);
delete from loan_product_requirement where id in (40009, 40010, 40011, 40012, 40013, 40014,
        41009, 41010, 41011, 41012, 41013, 41014, 42009, 42010, 42011, 42012, 42013, 42014);