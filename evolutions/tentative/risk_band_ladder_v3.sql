update loan_product_dependency set param2 = 2 where rule like 'rb_default_0%v2'  and type ='LOAN' and SUBSTR(rule, 15,1 ) > '0' ; 
update loan_product_dependency set target = '15000_750_3MONTH_v2', rule='rb_default_0504_15000_750_3MONTH_v2'  where id in (1159, 1160); 
update loan_product_dependency set target = '15000_600_3MONTH_v2', rule='rb_default_0404_15000_600_3MONTH_v2'  where id in (1119, 1120); 
update loan_product_dependency set target = '15000_750_3MONTH_v2', rule='rb_default_0404_15000_750_3MONTH_v2'  where id in (1169, 1170); 

insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_3000_300_2WEEK_v2_6_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '3000_300_2WEEK_v2', '1', NULL, NULL, 'false', now(), now(), '3000_300_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_3000_300_2WEEK_v2_6_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '6',  NULL, NULL, NULL, NULL, now(), now(), '3000_300_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_3000_360_2WEEK_v2_6_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '3000_360_2WEEK_v2', '1', NULL, NULL, 'false', now(), now(), '3000_360_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_3000_360_2WEEK_v2_6_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '6',  NULL, NULL, NULL, NULL, now(), now(), '3000_360_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_4000_320_2WEEK_v2_5_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '4000_320_2WEEK_v2', '1', NULL, NULL, 'false', now(), now(), '4000_320_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_4000_320_2WEEK_v2_5_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '5',  NULL, NULL, NULL, NULL, now(), now(), '4000_320_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_4000_400_2WEEK_v2_5_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '4000_400_2WEEK_v2', '1', NULL, NULL, 'false', now(), now(), '4000_400_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_4000_400_2WEEK_v2_5_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '5',  NULL, NULL, NULL, NULL, now(), now(), '4000_400_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_5000_400_2WEEK_v2_4_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '5000_400_2WEEK_v2', '1', NULL, NULL, 'false', now(), now(), '5000_400_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_5000_400_2WEEK_v2_4_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '4',  NULL, NULL, NULL, NULL, now(), now(), '5000_400_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_5000_400_2WEEK_v2_6_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '5000_400_2WEEK_v2', '1', NULL, NULL, 'false', now(), now(), '5000_400_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_5000_400_2WEEK_v2_6_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '6',  NULL, NULL, NULL, NULL, now(), now(), '5000_400_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_5000_500_2WEEK_v2_4_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '5000_500_2WEEK_v2', '1', NULL, NULL, 'false', now(), now(), '5000_500_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_5000_500_2WEEK_v2_4_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '4',  NULL, NULL, NULL, NULL, now(), now(), '5000_500_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_5000_500_2WEEK_v2_6_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '5000_500_2WEEK_v2', '1', NULL, NULL, 'false', now(), now(), '5000_500_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_5000_500_2WEEK_v2_6_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '6',  NULL, NULL, NULL, NULL, now(), now(), '5000_500_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_6000_360_2WEEK_v2_5_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '6000_360_2WEEK_v2', '1', NULL, NULL, 'false', now(), now(), '6000_360_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_6000_360_2WEEK_v2_5_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '5',  NULL, NULL, NULL, NULL, now(), now(), '6000_360_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_6000_420_2WEEK_v2_5_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '6000_420_2WEEK_v2', '1', NULL, NULL, 'false', now(), now(), '6000_420_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_6000_420_2WEEK_v2_5_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '5',  NULL, NULL, NULL, NULL, now(), now(), '6000_420_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_6000_480_2WEEK_v2_1_2_3_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '6000_480_2WEEK_v2', '1', NULL, NULL, 'false', now(), now(), '6000_480_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_6000_480_2WEEK_v2_1_2_3_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '1,2,3',  NULL, NULL, NULL, NULL, now(), now(), '6000_480_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_6000_600_2WEEK_v2_1_2_3_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '6000_600_2WEEK_v2', '1', NULL, NULL, 'false', now(), now(), '6000_600_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_6000_600_2WEEK_v2_1_2_3_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '1,2,3',  NULL, NULL, NULL, NULL, now(), now(), '6000_600_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_7000_420_1MONTH_v2_1_2_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '7000_420_1MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '7000_420_1MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_7000_420_1MONTH_v2_1_2_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '1,2',  NULL, NULL, NULL, NULL, now(), now(), '7000_420_1MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_7000_420_1MONTH_v2_4_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '7000_420_1MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '7000_420_1MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_7000_420_1MONTH_v2_4_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '4',  NULL, NULL, NULL, NULL, now(), now(), '7000_420_1MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_7000_490_1MONTH_v2_1_2_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '7000_490_1MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '7000_490_1MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_7000_490_1MONTH_v2_1_2_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '1,2',  NULL, NULL, NULL, NULL, now(), now(), '7000_490_1MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_7000_490_1MONTH_v2_4_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '7000_490_1MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '7000_490_1MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_7000_490_1MONTH_v2_4_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '4',  NULL, NULL, NULL, NULL, now(), now(), '7000_490_1MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_7000_490_1MONTH_v2_6_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '7000_490_1MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '7000_490_1MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_7000_490_1MONTH_v2_6_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '6',  NULL, NULL, NULL, NULL, now(), now(), '7000_490_1MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_7000_560_1MONTH_v2_6_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '7000_560_1MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '7000_560_1MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_7000_560_1MONTH_v2_6_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '6',  NULL, NULL, NULL, NULL, now(), now(), '7000_560_1MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_10000_400_2MONTH_v2_6_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '10000_400_2MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '10000_400_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_10000_400_2MONTH_v2_6_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '6',  NULL, NULL, NULL, NULL, now(), now(), '10000_400_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_10000_500_2MONTH_v2_1_2_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '10000_500_2MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '10000_500_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_10000_500_2MONTH_v2_1_2_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '1,2',  NULL, NULL, NULL, NULL, now(), now(), '10000_500_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_10000_500_2MONTH_v2_4_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '10000_500_2MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '10000_500_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_10000_500_2MONTH_v2_4_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '4',  NULL, NULL, NULL, NULL, now(), now(), '10000_500_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_10000_500_2MONTH_v2_6_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '10000_500_2MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '10000_500_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_10000_500_2MONTH_v2_6_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '6',  NULL, NULL, NULL, NULL, now(), now(), '10000_500_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_10000_600_2MONTH_v2_1_2_3_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '10000_600_2MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '10000_600_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_10000_600_2MONTH_v2_1_2_3_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '1,2,3',  NULL, NULL, NULL, NULL, now(), now(), '10000_600_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_10000_600_2MONTH_v2_4_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '10000_600_2MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '10000_600_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_10000_600_2MONTH_v2_4_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '4',  NULL, NULL, NULL, NULL, now(), now(), '10000_600_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_10000_600_2MONTH_v2_5_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '10000_600_2MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '10000_600_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_10000_600_2MONTH_v2_5_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '5',  NULL, NULL, NULL, NULL, now(), now(), '10000_600_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_10000_700_2MONTH_v2_5_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '10000_700_2MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '10000_700_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_10000_700_2MONTH_v2_5_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '5',  NULL, NULL, NULL, NULL, now(), now(), '10000_700_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_10000_800_2MONTH_v2_1_2_3_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '10000_800_2MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '10000_800_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_10000_800_2MONTH_v2_1_2_3_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '1,2,3',  NULL, NULL, NULL, NULL, now(), now(), '10000_800_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_15000_600_3MONTH_v2_4_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '15000_600_3MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '15000_600_3MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_15000_600_3MONTH_v2_4_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '4',  NULL, NULL, NULL, NULL, now(), now(), '15000_600_3MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_15000_450_3MONTH_v2_1_2_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '15000_450_3MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '15000_450_3MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_15000_450_3MONTH_v2_1_2_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '1,2',  NULL, NULL, NULL, NULL, now(), now(), '15000_450_3MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_15000_750_3MONTH_v2_5_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '15000_750_3MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '15000_750_3MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_15000_750_3MONTH_v2_5_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '5',  NULL, NULL, NULL, NULL, now(), now(), '15000_750_3MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_15000_600_3MONTH_v2_3_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '15000_600_3MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '15000_600_3MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_15000_600_3MONTH_v2_3_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '3',  NULL, NULL, NULL, NULL, now(), now(), '15000_600_3MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_15000_750_3MONTH_v2_3_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '15000_750_3MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '15000_750_3MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_15000_750_3MONTH_v2_3_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '3',  NULL, NULL, NULL, NULL, now(), now(), '15000_750_3MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_15000_750_3MONTH_v2_4_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '15000_750_3MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '15000_750_3MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_15000_750_3MONTH_v2_4_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '4',  NULL, NULL, NULL, NULL, now(), now(), '15000_750_3MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_15000_900_3MONTH_v2_5_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '15000_900_3MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '15000_900_3MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_15000_900_3MONTH_v2_5_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '5',  NULL, NULL, NULL, NULL, now(), now(), '15000_900_3MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_25000_250_4MONTH_v2_1_2_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '25000_250_4MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '25000_250_4MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_25000_250_4MONTH_v2_1_2_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '1,2',  NULL, NULL, NULL, NULL, now(), now(), '25000_250_4MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_25000_500_4MONTH_v2_1_2_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '25000_500_4MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '25000_500_4MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_25000_500_4MONTH_v2_1_2_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '1,2',  NULL, NULL, NULL, NULL, now(), now(), '25000_500_4MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_25000_500_4MONTH_v2_3_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '25000_500_4MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '25000_500_4MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_25000_500_4MONTH_v2_3_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '3',  NULL, NULL, NULL, NULL, now(), now(), '25000_500_4MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_25000_750_4MONTH_v2_3_repeat_unlocked', 'r1_lp', 'LOAN_PRODUCT_NAME', '25000_750_4MONTH_v2', '1', NULL, NULL, 'false', now(), now(), '25000_750_4MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
	 enabled, description) values ('rb_default_self_25000_750_4MONTH_v2_3_repeat_unlocked', 'r2_riskband', 'RISK_BAND', '3',  NULL, NULL, NULL, NULL, now(), now(), '25000_750_4MONTH_v2', 1, '  ');
