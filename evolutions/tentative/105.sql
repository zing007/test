# ---!Ups

update waiting_criteria set enabled = 1 where not(rule = 'activeLoanWithinFamily') and (rule like 'default%Link%');
update  loan_product set end_date = '2017-11-14'  where
        name in ('WAITVLP_4000_200_1MONTH', 'WAITVLP_6000_300_1MONTH', 'WAITVLP_10000_200_2MONTH');

# ---!Downs

update waiting_criteria set enabled = 0 where not(rule = 'activeLoanWithinFamily') and (rule like 'default%Link%');
update loan_product set end_date = '2027-06-20'  where
    name in ('WAITVLP_4000_200_1MONTH', 'WAITVLP_6000_300_1MONTH', 'WAITVLP_10000_200_2MONTH');