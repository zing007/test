insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (1120, 1000, 3, 120, 
     1, 2, 'WEEK', '2018-07-20', '2027-07-20', now(), now(), '1000_120_2WEEK_v2', 1120, 'loan_product_pricing_v2', 8);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0600_1000_120_2WEEK_v2', 'r1', 'MVEL_EXPRESSION', '1 == 1', '1', NULL, NULL, NULL, now(), now(), '1000_120_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0600_1000_120_2WEEK_v2', 'r2', 'RISK_BAND', '6', NULL, NULL, NULL, NULL, now(), now(), '1000_120_2WEEK_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (1120, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (1120, 18, '2018-07-20', now()); 
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (3120, 3000, 3, 360, 
     1, 2, 'WEEK', '2018-07-20', '2027-07-20', now(), now(), '3000_360_2WEEK_v2', 3120, 'loan_product_pricing_v2', 8);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0601_3000_360_2WEEK_v2', 'r1', 'LOAN', '1000', '1', NULL, NULL, NULL, now(), now(), '3000_360_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0601_3000_360_2WEEK_v2', 'r2', 'RISK_BAND', '6', NULL, NULL, NULL, NULL, now(), now(), '3000_360_2WEEK_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (3120, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (3120, 18, '2018-07-20', now()); 
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (5100, 5000, 3, 500, 
     1, 2, 'WEEK', '2018-07-20', '2027-07-20', now(), now(), '5000_500_2WEEK_v2', 5100, 'loan_product_pricing_v2', 8);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0602_5000_500_2WEEK_v2', 'r1', 'LOAN', '3000', '1', NULL, NULL, NULL, now(), now(), '5000_500_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0602_5000_500_2WEEK_v2', 'r2', 'RISK_BAND', '6', NULL, NULL, NULL, NULL, now(), now(), '5000_500_2WEEK_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (5100, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (5100, 18, '2018-07-20', now()); 
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (7080, 7000, 3, 560, 
     1, 1, 'MONTH', '2018-07-20', '2027-07-20', now(), now(), '7000_560_1MONTH_v2', 7080, 'loan_product_pricing_v2', 8);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0603_7000_560_1MONTH_v2', 'r1', 'LOAN', '5000', '1', NULL, NULL, NULL, now(), now(), '7000_560_1MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0603_7000_560_1MONTH_v2', 'r2', 'RISK_BAND', '6', NULL, NULL, NULL, NULL, now(), now(), '7000_560_1MONTH_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (7080, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (7080, 18, '2018-07-20', now()); 
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (10050, 10000, 3, 500, 
     2, 2, 'MONTH', '2018-07-20', '2027-07-20', now(), now(), '10000_500_2MONTH_v2', 10050, 'loan_product_pricing_v2', 8);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0604_10000_500_2MONTH_v2', 'r1', 'LOAN', '7000', '1', NULL, NULL, NULL, now(), now(), '10000_500_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0604_10000_500_2MONTH_v2', 'r2', 'RISK_BAND', '6', NULL, NULL, NULL, NULL, now(), now(), '10000_500_2MONTH_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (10050, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (10050, 18, '2018-07-20', now()); 
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (2120, 2000, 3, 240, 
     1, 2, 'WEEK', '2018-07-20', '2027-07-20', now(), now(), '2000_240_2WEEK_v2', 2120, 'loan_product_pricing_v2', 8);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0500_2000_240_2WEEK_v2', 'r1', 'MVEL_EXPRESSION', '1 == 1', '1', NULL, NULL, NULL, now(), now(), '2000_240_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0500_2000_240_2WEEK_v2', 'r2', 'RISK_BAND', '5', NULL, NULL, NULL, NULL, now(), now(), '2000_240_2WEEK_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (2120, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (2120, 18, '2018-07-20', now()); 
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (4100, 4000, 3, 400, 
     1, 2, 'WEEK', '2018-07-20', '2027-07-20', now(), now(), '4000_400_2WEEK_v2', 4100, 'loan_product_pricing_v2', 8);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0501_4000_400_2WEEK_v2', 'r1', 'LOAN', '2000', '1', NULL, NULL, NULL, now(), now(), '4000_400_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0501_4000_400_2WEEK_v2', 'r2', 'RISK_BAND', '5', NULL, NULL, NULL, NULL, now(), now(), '4000_400_2WEEK_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (4100, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (4100, 18, '2018-07-20', now()); 
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (6070, 6000, 3, 420, 
     1, 2, 'WEEK', '2018-07-20', '2027-07-20', now(), now(), '6000_420_2WEEK_v2', 6070, 'loan_product_pricing_v2', 8);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0502_6000_420_2WEEK_v2', 'r1', 'LOAN', '4000', '1', NULL, NULL, NULL, now(), now(), '6000_420_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0502_6000_420_2WEEK_v2', 'r2', 'RISK_BAND', '5', NULL, NULL, NULL, NULL, now(), now(), '6000_420_2WEEK_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (6070, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (6070, 18, '2018-07-20', now()); 
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (10070, 10000, 3, 700, 
     2, 2, 'MONTH', '2018-07-20', '2027-07-20', now(), now(), '10000_700_2MONTH_v2', 10070, 'loan_product_pricing_v2', 8);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0503_10000_700_2MONTH_v2', 'r1', 'LOAN', '6000', '1', NULL, NULL, NULL, now(), now(), '10000_700_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0503_10000_700_2MONTH_v2', 'r2', 'RISK_BAND', '5', NULL, NULL, NULL, NULL, now(), now(), '10000_700_2MONTH_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (10070, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (10070, 18, '2018-07-20', now()); 
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (15060, 15000, 3, 900, 
     3, 3, 'MONTH', '2018-07-20', '2027-07-20', now(), now(), '15000_900_3MONTH_v2', 15060, 'loan_product_pricing_v2', 8);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0504_15000_900_3MONTH_v2', 'r1', 'LOAN', '10000', '1', NULL, NULL, NULL, now(), now(), '15000_900_3MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0504_15000_900_3MONTH_v2', 'r2', 'RISK_BAND', '5', NULL, NULL, NULL, NULL, now(), now(), '15000_900_3MONTH_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (15060, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (15060, 18, '2018-07-20', now()); 
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (3100, 3000, 3, 300, 
     1, 2, 'WEEK', '2018-07-20', '2027-07-20', now(), now(), '3000_300_2WEEK_v2', 3100, 'loan_product_pricing_v2', 8);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0400_3000_300_2WEEK_v2', 'r1', 'MVEL_EXPRESSION', '1 == 1', '1', NULL, NULL, NULL, now(), now(), '3000_300_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0400_3000_300_2WEEK_v2', 'r2', 'RISK_BAND', '4', NULL, NULL, NULL, NULL, now(), now(), '3000_300_2WEEK_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (3100, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (3100, 18, '2018-07-20', now()); 
 
-- loan_product 5000_500_2WEEK_v2 has already been inserted
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0401_5000_500_2WEEK_v2', 'r1', 'LOAN', '3000', '1', NULL, NULL, NULL, now(), now(), '5000_500_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0401_5000_500_2WEEK_v2', 'r2', 'RISK_BAND', '4', NULL, NULL, NULL, NULL, now(), now(), '5000_500_2WEEK_v2', 1, '  ');
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (7070, 7000, 3, 490, 
     1, 1, 'MONTH', '2018-07-20', '2027-07-20', now(), now(), '7000_490_1MONTH_v2', 7070, 'loan_product_pricing_v2', 8);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0402_7000_490_1MONTH_v2', 'r1', 'LOAN', '5000', '1', NULL, NULL, NULL, now(), now(), '7000_490_1MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0402_7000_490_1MONTH_v2', 'r2', 'RISK_BAND', '4', NULL, NULL, NULL, NULL, now(), now(), '7000_490_1MONTH_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (7070, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (7070, 18, '2018-07-20', now()); 
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (10060, 10000, 3, 600, 
     2, 2, 'MONTH', '2018-07-20', '2027-07-20', now(), now(), '10000_600_2MONTH_v2', 10060, 'loan_product_pricing_v2', 8);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0403_10000_600_2MONTH_v2', 'r1', 'LOAN', '7000', '1', NULL, NULL, NULL, now(), now(), '10000_600_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0403_10000_600_2MONTH_v2', 'r2', 'RISK_BAND', '4', NULL, NULL, NULL, NULL, now(), now(), '10000_600_2MONTH_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (10060, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (10060, 18, '2018-07-20', now()); 
 
-- loan_product 15000_900_3MONTH_v2 has already been inserted
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0404_15000_900_3MONTH_v2', 'r1', 'LOAN', '10000', '1', NULL, NULL, NULL, now(), now(), '15000_900_3MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0404_15000_900_3MONTH_v2', 'r2', 'RISK_BAND', '4', NULL, NULL, NULL, NULL, now(), now(), '15000_900_3MONTH_v2', 1, '  ');
 
-- loan_product 4000_400_2WEEK_v2 has already been inserted
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0300_4000_400_2WEEK_v2', 'r1', 'MVEL_EXPRESSION', '1 == 1', '1', NULL, NULL, NULL, now(), now(), '4000_400_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0300_4000_400_2WEEK_v2', 'r2', 'RISK_BAND', '3', NULL, NULL, NULL, NULL, now(), now(), '4000_400_2WEEK_v2', 1, '  ');
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (6100, 6000, 3, 600, 
     1, 2, 'WEEK', '2018-07-20', '2027-07-20', now(), now(), '6000_600_2WEEK_v2', 6100, 'loan_product_pricing_v2', 8);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0301_6000_600_2WEEK_v2', 'r1', 'LOAN', '4000', '1', NULL, NULL, NULL, now(), now(), '6000_600_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0301_6000_600_2WEEK_v2', 'r2', 'RISK_BAND', '3', NULL, NULL, NULL, NULL, now(), now(), '6000_600_2WEEK_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (6100, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (6100, 18, '2018-07-20', now()); 
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (10080, 10000, 3, 800, 
     2, 2, 'MONTH', '2018-07-20', '2027-07-20', now(), now(), '10000_800_2MONTH_v2', 10080, 'loan_product_pricing_v2', 8);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0302_10000_800_2MONTH_v2', 'r1', 'LOAN', '6000', '1', NULL, NULL, NULL, now(), now(), '10000_800_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0302_10000_800_2MONTH_v2', 'r2', 'RISK_BAND', '3', NULL, NULL, NULL, NULL, now(), now(), '10000_800_2MONTH_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (10080, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (10080, 18, '2018-07-20', now()); 
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (15050, 15000, 3, 750, 
     3, 3, 'MONTH', '2018-07-20', '2027-07-20', now(), now(), '15000_750_3MONTH_v2', 15050, 'loan_product_pricing_v2', 8);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0303_15000_750_3MONTH_v2', 'r1', 'LOAN', '10000', '1', NULL, NULL, NULL, now(), now(), '15000_750_3MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0303_15000_750_3MONTH_v2', 'r2', 'RISK_BAND', '3', NULL, NULL, NULL, NULL, now(), now(), '15000_750_3MONTH_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (15050, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (15050, 18, '2018-07-20', now()); 
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (25030, 25000, 3, 750, 
     4, 4, 'MONTH', '2018-07-20', '2027-07-20', now(), now(), '25000_750_4MONTH_v2', 25030, 'loan_product_pricing_v2', 8);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0304_25000_750_4MONTH_v2', 'r1', 'LOAN', '15000', '1', NULL, NULL, NULL, now(), now(), '25000_750_4MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0304_25000_750_4MONTH_v2', 'r2', 'RISK_BAND', '3', NULL, NULL, NULL, NULL, now(), now(), '25000_750_4MONTH_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (25030, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (25030, 18, '2018-07-20', now()); 
 
-- loan_product 5000_500_2WEEK_v2 has already been inserted
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0100_5000_500_2WEEK_v2', 'r1', 'MVEL_EXPRESSION', '1 == 1', '1', NULL, NULL, NULL, now(), now(), '5000_500_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0100_5000_500_2WEEK_v2', 'r2', 'RISK_BAND', '1,2', NULL, NULL, NULL, NULL, now(), now(), '5000_500_2WEEK_v2', 1, '  ');
 
-- loan_product 7000_490_1MONTH_v2 has already been inserted
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0101_7000_490_1MONTH_v2', 'r1', 'LOAN_PRODUCT_NAME', '5000_500_2WEEK_v2', '1', NULL, NULL, NULL, now(), now(), '7000_490_1MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0101_7000_490_1MONTH_v2', 'r2', 'RISK_BAND', '1,2', NULL, NULL, NULL, NULL, now(), now(), '7000_490_1MONTH_v2', 1, '  ');
 
-- loan_product 10000_500_2MONTH_v2 has already been inserted
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0102_10000_500_2MONTH_v2', 'r1', 'LOAN', '7000', '1', NULL, NULL, NULL, now(), now(), '10000_500_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0102_10000_500_2MONTH_v2', 'r2', 'RISK_BAND', '1,2', NULL, NULL, NULL, NULL, now(), now(), '10000_500_2MONTH_v2', 1, '  ');
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (15030, 15000, 3, 450, 
     3, 3, 'MONTH', '2018-07-20', '2027-07-20', now(), now(), '15000_450_3MONTH_v2', 15030, 'loan_product_pricing_v2', 8);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0103_15000_450_3MONTH_v2', 'r1', 'LOAN', '10000', '1', NULL, NULL, NULL, now(), now(), '15000_450_3MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0103_15000_450_3MONTH_v2', 'r2', 'RISK_BAND', '1,2', NULL, NULL, NULL, NULL, now(), now(), '15000_450_3MONTH_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (15030, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (15030, 18, '2018-07-20', now()); 
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (25020, 25000, 3, 500, 
     4, 4, 'MONTH', '2018-07-20', '2027-07-20', now(), now(), '25000_500_4MONTH_v2', 25020, 'loan_product_pricing_v2', 8);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0104_25000_500_4MONTH_v2', 'r1', 'LOAN', '15000', '1', NULL, NULL, NULL, now(), now(), '25000_500_4MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0104_25000_500_4MONTH_v2', 'r2', 'RISK_BAND', '1,2', NULL, NULL, NULL, NULL, now(), now(), '25000_500_4MONTH_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (25020, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (25020, 18, '2018-07-20', now()); 
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (1100, 1000, 3, 100, 
     1, 2, 'WEEK', '2018-07-20', '2027-07-20', now(), now(), '1000_100_2WEEK_v2', 1100, 'loan_product_pricing_v2', 9);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0600_1000_100_2WEEK_v2', 'r1', 'MVEL_EXPRESSION', '1 == 1', '1', NULL, NULL, NULL, now(), now(), '1000_100_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0600_1000_100_2WEEK_v2', 'r2', 'RISK_BAND', '6', NULL, NULL, NULL, NULL, now(), now(), '1000_100_2WEEK_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (1100, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (1100, 18, '2018-07-20', now()); 
 
-- loan_product 3000_300_2WEEK_v2 has already been inserted
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0601_3000_300_2WEEK_v2', 'r1', 'LOAN', '1000', '1', NULL, NULL, NULL, now(), now(), '3000_300_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0601_3000_300_2WEEK_v2', 'r2', 'RISK_BAND', '6', NULL, NULL, NULL, NULL, now(), now(), '3000_300_2WEEK_v2', 1, '  ');
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (5080, 5000, 3, 400, 
     1, 2, 'WEEK', '2018-07-20', '2027-07-20', now(), now(), '5000_400_2WEEK_v2', 5080, 'loan_product_pricing_v2', 9);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0602_5000_400_2WEEK_v2', 'r1', 'LOAN', '3000', '1', NULL, NULL, NULL, now(), now(), '5000_400_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0602_5000_400_2WEEK_v2', 'r2', 'RISK_BAND', '6', NULL, NULL, NULL, NULL, now(), now(), '5000_400_2WEEK_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (5080, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (5080, 18, '2018-07-20', now()); 
 
-- loan_product 7000_490_1MONTH_v2 has already been inserted
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0603_7000_490_1MONTH_v2', 'r1', 'LOAN', '5000', '1', NULL, NULL, NULL, now(), now(), '7000_490_1MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0603_7000_490_1MONTH_v2', 'r2', 'RISK_BAND', '6', NULL, NULL, NULL, NULL, now(), now(), '7000_490_1MONTH_v2', 1, '  ');
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (10040, 10000, 3, 400, 
     2, 2, 'MONTH', '2018-07-20', '2027-07-20', now(), now(), '10000_400_2MONTH_v2', 10040, 'loan_product_pricing_v2', 9);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0604_10000_400_2MONTH_v2', 'r1', 'LOAN', '7000', '1', NULL, NULL, NULL, now(), now(), '10000_400_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0604_10000_400_2MONTH_v2', 'r2', 'RISK_BAND', '6', NULL, NULL, NULL, NULL, now(), now(), '10000_400_2MONTH_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (10040, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (10040, 18, '2018-07-20', now()); 
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (2100, 2000, 3, 200, 
     1, 2, 'WEEK', '2018-07-20', '2027-07-20', now(), now(), '2000_200_2WEEK_v2', 2100, 'loan_product_pricing_v2', 9);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0500_2000_200_2WEEK_v2', 'r1', 'MVEL_EXPRESSION', '1 == 1', '1', NULL, NULL, NULL, now(), now(), '2000_200_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0500_2000_200_2WEEK_v2', 'r2', 'RISK_BAND', '5', NULL, NULL, NULL, NULL, now(), now(), '2000_200_2WEEK_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (2100, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (2100, 18, '2018-07-20', now()); 
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (4080, 4000, 3, 320, 
     1, 2, 'WEEK', '2018-07-20', '2027-07-20', now(), now(), '4000_320_2WEEK_v2', 4080, 'loan_product_pricing_v2', 9);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0501_4000_320_2WEEK_v2', 'r1', 'LOAN', '2000', '1', NULL, NULL, NULL, now(), now(), '4000_320_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0501_4000_320_2WEEK_v2', 'r2', 'RISK_BAND', '5', NULL, NULL, NULL, NULL, now(), now(), '4000_320_2WEEK_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (4080, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (4080, 18, '2018-07-20', now()); 
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (6060, 6000, 3, 360, 
     1, 2, 'WEEK', '2018-07-20', '2027-07-20', now(), now(), '6000_360_2WEEK_v2', 6060, 'loan_product_pricing_v2', 9);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0502_6000_360_2WEEK_v2', 'r1', 'LOAN', '4000', '1', NULL, NULL, NULL, now(), now(), '6000_360_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0502_6000_360_2WEEK_v2', 'r2', 'RISK_BAND', '5', NULL, NULL, NULL, NULL, now(), now(), '6000_360_2WEEK_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (6060, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (6060, 18, '2018-07-20', now()); 
 
-- loan_product 10000_600_2MONTH_v2 has already been inserted
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0503_10000_600_2MONTH_v2', 'r1', 'LOAN', '6000', '1', NULL, NULL, NULL, now(), now(), '10000_600_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0503_10000_600_2MONTH_v2', 'r2', 'RISK_BAND', '5', NULL, NULL, NULL, NULL, now(), now(), '10000_600_2MONTH_v2', 1, '  ');
 
-- loan_product 15000_450_3MONTH_v2 has already been inserted
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0504_15000_450_3MONTH_v2', 'r1', 'LOAN', '10000', '1', NULL, NULL, NULL, now(), now(), '15000_450_3MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0504_15000_450_3MONTH_v2', 'r2', 'RISK_BAND', '5', NULL, NULL, NULL, NULL, now(), now(), '15000_450_3MONTH_v2', 1, '  ');
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (3080, 3000, 3, 240, 
     1, 2, 'WEEK', '2018-07-20', '2027-07-20', now(), now(), '3000_240_2WEEK_v2', 3080, 'loan_product_pricing_v2', 9);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0400_3000_240_2WEEK_v2', 'r1', 'MVEL_EXPRESSION', '1 == 1', '1', NULL, NULL, NULL, now(), now(), '3000_240_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0400_3000_240_2WEEK_v2', 'r2', 'RISK_BAND', '4', NULL, NULL, NULL, NULL, now(), now(), '3000_240_2WEEK_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (3080, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (3080, 18, '2018-07-20', now()); 
 
-- loan_product 5000_400_2WEEK_v2 has already been inserted
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0401_5000_400_2WEEK_v2', 'r1', 'LOAN', '3000', '1', NULL, NULL, NULL, now(), now(), '5000_400_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0401_5000_400_2WEEK_v2', 'r2', 'RISK_BAND', '4', NULL, NULL, NULL, NULL, now(), now(), '5000_400_2WEEK_v2', 1, '  ');
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (7060, 7000, 3, 420, 
     1, 1, 'MONTH', '2018-07-20', '2027-07-20', now(), now(), '7000_420_1MONTH_v2', 7060, 'loan_product_pricing_v2', 9);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0402_7000_420_1MONTH_v2', 'r1', 'LOAN', '5000', '1', NULL, NULL, NULL, now(), now(), '7000_420_1MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0402_7000_420_1MONTH_v2', 'r2', 'RISK_BAND', '4', NULL, NULL, NULL, NULL, now(), now(), '7000_420_1MONTH_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (7060, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (7060, 18, '2018-07-20', now()); 
 
-- loan_product 10000_500_2MONTH_v2 has already been inserted
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0403_10000_500_2MONTH_v2', 'r1', 'LOAN', '7000', '1', NULL, NULL, NULL, now(), now(), '10000_500_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0403_10000_500_2MONTH_v2', 'r2', 'RISK_BAND', '4', NULL, NULL, NULL, NULL, now(), now(), '10000_500_2MONTH_v2', 1, '  ');
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (15020, 15000, 3, 300, 
     3, 3, 'MONTH', '2018-07-20', '2027-07-20', now(), now(), '15000_300_3MONTH_v2', 15020, 'loan_product_pricing_v2', 9);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0404_15000_300_3MONTH_v2', 'r1', 'LOAN', '10000', '1', NULL, NULL, NULL, now(), now(), '15000_300_3MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0404_15000_300_3MONTH_v2', 'r2', 'RISK_BAND', '4', NULL, NULL, NULL, NULL, now(), now(), '15000_300_3MONTH_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (15020, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (15020, 18, '2018-07-20', now()); 
 
-- loan_product 4000_320_2WEEK_v2 has already been inserted
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0300_4000_320_2WEEK_v2', 'r1', 'MVEL_EXPRESSION', '1 == 1', '1', NULL, NULL, NULL, now(), now(), '4000_320_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0300_4000_320_2WEEK_v2', 'r2', 'RISK_BAND', '3', NULL, NULL, NULL, NULL, now(), now(), '4000_320_2WEEK_v2', 1, '  ');
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (6080, 6000, 3, 480, 
     1, 2, 'WEEK', '2018-07-20', '2027-07-20', now(), now(), '6000_480_2WEEK_v2', 6080, 'loan_product_pricing_v2', 9);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0301_6000_480_2WEEK_v2', 'r1', 'LOAN', '4000', '1', NULL, NULL, NULL, now(), now(), '6000_480_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0301_6000_480_2WEEK_v2', 'r2', 'RISK_BAND', '3', NULL, NULL, NULL, NULL, now(), now(), '6000_480_2WEEK_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (6080, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (6080, 18, '2018-07-20', now()); 
 
-- loan_product 10000_600_2MONTH_v2 has already been inserted
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0302_10000_600_2MONTH_v2', 'r1', 'LOAN', '6000', '1', NULL, NULL, NULL, now(), now(), '10000_600_2MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0302_10000_600_2MONTH_v2', 'r2', 'RISK_BAND', '3', NULL, NULL, NULL, NULL, now(), now(), '10000_600_2MONTH_v2', 1, '  ');
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (15040, 15000, 3, 600, 
     3, 3, 'MONTH', '2018-07-20', '2027-07-20', now(), now(), '15000_600_3MONTH_v2', 15040, 'loan_product_pricing_v2', 9);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0303_15000_600_3MONTH_v2', 'r1', 'LOAN', '10000', '1', NULL, NULL, NULL, now(), now(), '15000_600_3MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0303_15000_600_3MONTH_v2', 'r2', 'RISK_BAND', '3', NULL, NULL, NULL, NULL, now(), now(), '15000_600_3MONTH_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (15040, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (15040, 18, '2018-07-20', now()); 
 
-- loan_product 25000_500_4MONTH_v2 has already been inserted
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0304_25000_500_4MONTH_v2', 'r1', 'LOAN', '15000', '1', NULL, NULL, NULL, now(), now(), '25000_500_4MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0304_25000_500_4MONTH_v2', 'r2', 'RISK_BAND', '3', NULL, NULL, NULL, NULL, now(), now(), '25000_500_4MONTH_v2', 1, '  ');
 
-- loan_product 5000_400_2WEEK_v2 has already been inserted
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0100_5000_400_2WEEK_v2', 'r1', 'MVEL_EXPRESSION', '1 == 1', '1', NULL, NULL, NULL, now(), now(), '5000_400_2WEEK_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0100_5000_400_2WEEK_v2', 'r2', 'RISK_BAND', '1,2', NULL, NULL, NULL, NULL, now(), now(), '5000_400_2WEEK_v2', 1, '  ');
 
-- loan_product 7000_420_1MONTH_v2 has already been inserted
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0101_7000_420_1MONTH_v2', 'r1', 'LOAN_PRODUCT_NAME', '5000_400_2WEEK_v2', '1', NULL, NULL, NULL, now(), now(), '7000_420_1MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0101_7000_420_1MONTH_v2', 'r2', 'RISK_BAND', '1,2', NULL, NULL, NULL, NULL, now(), now(), '7000_420_1MONTH_v2', 1, '  ');
 
-- loan_product 10000_500_2MONTH_v2 has already been inserted
 
-- loan_product 15000_450_3MONTH_v2 has already been inserted
 
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date, 
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (25010, 25000, 3, 250, 
     4, 4, 'MONTH', '2018-07-20', '2027-07-20', now(), now(), '25000_250_4MONTH_v2', 25010, 'loan_product_pricing_v2', 9);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0104_25000_250_4MONTH_v2', 'r1', 'LOAN', '15000', '1', NULL, NULL, NULL, now(), now(), '25000_250_4MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target, 
    enabled, description) values ('rb_default_0104_25000_250_4MONTH_v2', 'r2', 'RISK_BAND', '1,2', NULL, NULL, NULL, NULL, now(), now(), '25000_250_4MONTH_v2', 1, '  ');
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (25010, 16, '2018-07-20', now()); 
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (25010, 18, '2018-07-20', now()); 
 
