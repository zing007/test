# ---!Ups

insert into ab_test_config values (8, 'use_new_ml_models', NULL, now(), now());
insert into ab_test values (8,  'use_new_ml_models', '2018-11-21 03:00:00',
    '2019-11-21', 'use new ml models for intent', now(), now());
insert into ab_test_group values (80001, 8,1, "0", 99, 'use old ml models', now(), now() );
insert into ab_test_group values (80002, 8, 2, "1", 1, 'use new ml models', now(), now() );


# ---!Downs

delete from ab_test_group  where ab_test_id in ( select id from ab_test
    where config_key = 'use_new_ml_models' );
delete from ab_test where config_key = 'use_new_ml_models';
delete from ab_test_config where property_key = 'use_new_ml_models'
