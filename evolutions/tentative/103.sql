# ---!Ups

update waiting_criteria set enabled = 0 where not(rule = 'activeLoanWithinFamily') and (rule like 'default%Link%');

# ---!Downs

update waiting_criteria set enabled = 1 where not(rule = 'activeLoanWithinFamily') and (rule like 'default%Link%');
