
update loan_product_dependency set `type`='LOAN', param1 = '2000', param2 = '1', param3 = '2', param4='WEEK'
  where target  = '3000_150_1MONTH' and `type`='LOAN_PRODUCT_NAME' and param1 = '2000_100_2WEEK';


insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (1001, 1000, 3, 80, 1, 2, 'WEEK', '2018-04-22',
       '2027-06-20', now(), now(), '1000_80_2WEEK', -1, 'loan_product_pricing', 1);
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (1002, 1000, 3, 99, 1, 2, 'WEEK', '2018-04-22',
       '2027-06-20', now(), now(), '1000_99_2WEEK', -1, 'loan_product_pricing', 2);
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (1003, 1000, 3, 120, 1, 2, 'WEEK', '2018-04-22',
       '2027-06-20', now(), now(), '1000_120_2WEEK', -1, 'loan_product_pricing', 3);
update loan_product set base_lp_id =id, group_key='loan_product_pricing', group_value=NULL where name = '1000_50_2WEEK';
update loan_product l1, loan_product l2 set l1.base_lp_id = l2.id where l2.name='1000_50_2WEEK' and l1.name
    in ('1000_80_2WEEK', '1000_99_2WEEK', '1000_120_2WEEK');


insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (2001, 2000, 3, 160, 1, 2, 'WEEK', '2018-04-22',
       '2027-06-20', now(), now(), '2000_160_2WEEK', -1, 'loan_product_pricing', 1);
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (2002, 2000, 3, 200, 1, 2, 'WEEK', '2018-04-22',
       '2027-06-20', now(), now(), '2000_200_2WEEK', -1, 'loan_product_pricing', 2);
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (2003, 2000, 3, 240, 1, 2, 'WEEK', '2018-04-22',
       '2027-06-20', now(), now(), '2000_240_2WEEK', -1, 'loan_product_pricing', 3);
update loan_product set base_lp_id =id,  group_key='loan_product_pricing', group_value=NULL where name = '2000_100_2WEEK';
update loan_product l1, loan_product l2 set l1.base_lp_id = l2.id where l2.name='2000_100_2WEEK' and l1.name
     in ('2000_160_2WEEK', '2000_200_2WEEK', '2000_240_2WEEK');


insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (3001, 3000, 3, 240, 1, 1, 'MONTH', '2018-04-22',
       '2027-06-20', now(), now(), '3000_240_1MONTH', -1, 'loan_product_pricing', 1);
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (3002, 3000, 3, 300, 1, 1, 'MONTH', '2018-04-22',
       '2027-06-20', now(), now(), '3000_300_1MONTH', -1, 'loan_product_pricing', 2);
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (3003, 3000, 3, 360, 1, 1, 'MONTH', '2018-04-22',
       '2027-06-20', now(), now(), '3000_360_1MONTH', -1, 'loan_product_pricing', 3);
update loan_product set base_lp_id =id,  group_key='loan_product_pricing', group_value=NULL where name = '3000_150_1MONTH';
update loan_product l1, loan_product l2 set l1.base_lp_id = l2.id where l2.name='3000_150_1MONTH' and l1.name
     in ('3000_240_1MONTH', '3000_300_1MONTH', '3000_360_1MONTH');


insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (4001, 4000, 3, 320, 1, 1, 'MONTH', '2018-04-22',
       '2027-06-20', now(), now(), '4000_320_1MONTH', -1, 'loan_product_pricing', 1);
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (4002, 4000, 3, 400, 1, 1, 'MONTH', '2018-04-22',
       '2027-06-20', now(), now(), '4000_400_1MONTH', -1, 'loan_product_pricing', 2);
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (4003, 4000, 3, 480, 1, 1, 'MONTH', '2018-04-22',
       '2027-06-20', now(), now(), '4000_480_1MONTH', -1, 'loan_product_pricing', 3);
update loan_product set base_lp_id =id,  group_key='loan_product_pricing', group_value=NULL where name = '4000_200_1MONTH';
update loan_product l1, loan_product l2 set l1.base_lp_id = l2.id where l2.name='4000_200_1MONTH' and l1.name
     in ('4000_320_1MONTH', '4000_400_1MONTH', '4000_480_1MONTH');


insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (5001, 5000, 3, 400, 1, 1, 'MONTH', '2018-04-22',
       '2027-06-20', now(), now(), '5000_400_1MONTH', -1, 'loan_product_pricing', 1);
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (5002, 5000, 3, 500, 1, 1, 'MONTH', '2018-04-22',
       '2027-06-20', now(), now(), '5000_500_1MONTH', -1, 'loan_product_pricing', 2);
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (5003, 5000, 3, 600, 1, 1, 'MONTH', '2018-04-22',
       '2027-06-20', now(), now(), '5000_600_1MONTH', -1, 'loan_product_pricing', 3);
update loan_product set base_lp_id =id,  group_key='loan_product_pricing', group_value=NULL where name = '5000_200_1MONTH';
update loan_product l1, loan_product l2 set l1.base_lp_id = l2.id where l2.name='5000_200_1MONTH' and l1.name
    in ('5000_400_1MONTH', '5000_500_1MONTH', '5000_600_1MONTH');

insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (6001, 6000, 3, 480, 1, 1, 'MONTH', '2018-04-22',
       '2027-06-20', now(), now(), '6000_480_1MONTH', -1, 'loan_product_pricing', 1);
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (6002, 6000, 3, 600, 1, 1, 'MONTH', '2018-04-22',
       '2027-06-20', now(), now(), '6000_600_1MONTH', -1, 'loan_product_pricing', 2);
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (6003, 6000, 3, 720, 1, 1, 'MONTH', '2018-04-22',
       '2027-06-20', now(), now(), '6000_720_1MONTH', -1, 'loan_product_pricing', 3);
update loan_product set base_lp_id =id,  group_key='loan_product_pricing', group_value=NULL where name = '6000_300_1MONTH';
update loan_product l1, loan_product l2 set l1.base_lp_id = l2.id where l2.name='6000_300_1MONTH' and l1.name
    in ('6000_480_1MONTH', '6000_600_1MONTH', '6000_720_1MONTH');


update loan_product set base_lp_id = id where base_lp_id is NULL;

insert into ab_test_config (property_key, property_value, created_at, updated_at) VALUES ('loan_product_pricing', NULL, now(), now());
insert into ab_test (id, config_key, start_date, end_date, description, created_at, updated_at) VALUES (4, 'loan_product_pricing', now(), now() + interval 2 year,'Loan Product Pricing Test', now(), now());
insert into ab_test_group (ab_test_id, bucket_id, value, weight, description, created_at, updated_at) VALUES (4, 1, NULL, 30,'30% - No Change', now(), now());
insert into ab_test_group (ab_test_id, bucket_id, value, weight, description, created_at, updated_at) VALUES (4, 2, 1, 25,   '25% at 8%', now(), now());
insert into ab_test_group (ab_test_id, bucket_id, value, weight, description, created_at, updated_at) VALUES (4, 3, 2, 25,   '25% at 10%', now(), now());
insert into ab_test_group (ab_test_id, bucket_id, value, weight, description, created_at, updated_at) VALUES (4, 4, 3, 20,   '15% at 12%', now(), now());






