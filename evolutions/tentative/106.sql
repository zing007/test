# ---!Ups

insert into ab_test_config values (1, 'ignore_docs_for_financial_capacity', NULL, now(), now());
insert into ab_test values (1,  'ignore_docs_for_financial_capacity', '2017-11-27 03:00:00',
    '2018-12-31', 'relax document requirement', now(), now());
insert into ab_test_group values (10001, 1,1, NULL, 20, 'relax nothing', now(), now() );
insert into ab_test_group values (10002, 1, 2, 'BANK_STMT,SALARY_SLIP__1M, SALARY_SLIP__2M,SALARY_SLIP__3M,CIBIL',
    20, 'relax few docs', now(), now() );
insert into ab_test_group values (10003, 1, 3, 'BANK_STMT,SALARY_SLIP__1M, SALARY_SLIP__2M,SALARY_SLIP__3M,CIBIL,PAN',
    60, 'relax few docs', now(), now() );
insert into ab_test_filter values (1, 1, 'NEW_USERS', 'only for new users', now(), now() );


insert into ab_test_config values (2, 'reject_user_with_low_intent_score', NULL, now(), now());
insert into ab_test values (2,  'reject_user_with_low_intent_score', '2018-01-04 03:00:00',
    '2018-12-31', 'reject with low intent score', now(), now());
insert into ab_test_group values (20001, 2, 1, "0", 50, 'do not reject', now(), now() );
insert into ab_test_group values (20002, 2, 2, "1", 50, 'reject',        now(), now() );


# ---!Downs

delete from ab_test_group  where ab_test_id in ( select id from ab_test
    where config_key = 'ignore_docs_for_financial_capacity' );
delete from ab_test_filter where ab_test_id in ( select id from ab_test
    where config_key = 'ignore_docs_for_financial_capacity' );
delete from ab_test where config_key = 'ignore_docs_for_financial_capacity';


delete from ab_test_group  where ab_test_id in ( select id from ab_test
    where config_key = 'reject_user_with_low_intent_score' );
delete from ab_test where config_key = 'reject_user_with_low_intent_score';
