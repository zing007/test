
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (1005, 1000, 3, 80, 1, 2, 'WEEK', '2018-04-22',
       '2027-06-20', now(), now(), '1000_80_2WEEK_V2', -1, 'loan_product_pricing', 5);
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (1006, 1000, 3, 120, 1, 2, 'WEEK', '2018-04-22',
       '2027-06-20', now(), now(), '1000_120_2WEEK_V2', -1, 'loan_product_pricing', 6);
update loan_product l1, loan_product l2 set l1.base_lp_id = l2.id where l2.name='1000_50_2WEEK' and l1.name
    in ('1000_80_2WEEK_V2', '1000_120_2WEEK_V2');
insert into loan_product_dependency (id, rule, sub_rule, `type`, param1, target, enabled, created_at, updated_at)
    values (80, 'default', '', 'MVEL_EXPRESSION', '1 == 1', '1000_80_2WEEK_V2', 1, now(), now() );
insert into loan_product_dependency (id, rule, sub_rule, `type`, param1, target, enabled, created_at, updated_at)
    values (81, 'default', '', 'MVEL_EXPRESSION', '1 == 1', '1000_120_2WEEK_V2', 1, now(), now() );

insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (2005, 2000, 3, 160, 1, 2, 'WEEK', '2018-04-22',
       '2027-06-20', now(), now(), '2000_160_2WEEK_V2', -1, 'loan_product_pricing', 5);
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (2006, 2000, 3, 240, 1, 2, 'WEEK', '2018-04-22',
       '2027-06-20', now(), now(), '2000_240_2WEEK_V2', -1, 'loan_product_pricing', 6);
update loan_product l1, loan_product l2 set l1.base_lp_id = l2.id where l2.name='2000_100_2WEEK' and l1.name
     in ('2000_160_2WEEK_V2', '2000_240_2WEEK_V2');
insert into loan_product_dependency (id, rule, sub_rule, `type`, param1, param2, param3, param4, target, enabled, created_at, updated_at)
    values (82, 'default', '', 'LOAN', '1000', '1', '2', 'WEEK', '2000_160_2WEEK_V2', 1, now(), now() );
insert into loan_product_dependency (id, rule, sub_rule, `type`, param1, param2, param3, param4, target, enabled, created_at, updated_at)
    values (83, 'default', '', 'LOAN', '1000', '1', '2', 'WEEK', '2000_240_2WEEK_V2', 1, now(), now() );


insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
    end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (3005, 3000, 3, 150, 1, 2, 'WEEK', '2018-04-22',
   '2027-06-20', now(), now(), '3000_150_2WEEK', -1, 'loan_product_pricing', 5);
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (3006, 3000, 3, 240, 1, 2, 'WEEK', '2018-04-22',
     '2027-06-20', now(), now(), '3000_240_2WEEK', -1, 'loan_product_pricing', 6);
update loan_product l1, loan_product l2 set l1.base_lp_id = l2.id where l2.name='3000_150_1MONTH' and l1.name
     in ('3000_150_2WEEK', '3000_240_2WEEK');


insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
    end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (4005, 4000, 3, 200, 1, 2, 'WEEK', '2018-04-22',
   '2027-06-20', now(), now(), '4000_200_2WEEK', -1, 'loan_product_pricing', 5);
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (4006, 4000, 3, 320, 1, 2, 'WEEK', '2018-04-22',
     '2027-06-20', now(), now(), '4000_320_2WEEK', -1, 'loan_product_pricing', 6);
update loan_product l1, loan_product l2 set l1.base_lp_id = l2.id where l2.name='4000_200_1MONTH' and l1.name
     in ('4000_200_2WEEK', '4000_320_2WEEK');


insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
    end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (5005, 5000, 3, 250, 1, 2, 'WEEK', '2018-04-22',
   '2027-06-20', now(), now(), '5000_250_2WEEK', -1, 'loan_product_pricing', 5);
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (5006, 5000, 3, 400, 1, 2, 'WEEK', '2018-04-22',
     '2027-06-20', now(), now(), '5000_400_2WEEK', -1, 'loan_product_pricing', 6);
update loan_product l1, loan_product l2 set l1.base_lp_id = l2.id where l2.name='5000_200_1MONTH' and l1.name
     in ('5000_250_2WEEK', '5000_400_2WEEK');


insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
    end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (6005, 6000, 3, 300, 1, 2, 'WEEK', '2018-04-22',
   '2027-06-20', now(), now(), '6000_300_2WEEK', -1, 'loan_product_pricing', 5);
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (6006, 6000, 3, 480, 1, 2, 'WEEK', '2018-04-22',
     '2027-06-20', now(), now(), '6000_480_2WEEK', -1, 'loan_product_pricing', 6);
update loan_product l1, loan_product l2 set l1.base_lp_id = l2.id where l2.name='6000_300_1MONTH' and l1.name
     in ('6000_300_2WEEK', '6000_480_2WEEK');


update ab_test_group set `value`=1, weight = 20, description='20% at 8% 1m' where ab_test_id = 4 and bucket_id = 2 ;
update ab_test_group set `value`=2, weight =  0, description='0% at 10% 1m' where ab_test_id = 4 and bucket_id = 3 ;
update ab_test_group set `value`=3, weight = 15, description='15% at 12% 1m' where ab_test_id = 4 and bucket_id = 4 ;
insert into ab_test_group (ab_test_id, bucket_id, value, weight, description, created_at, updated_at) VALUES (4, 5, 5, 20,   '20% at 5% 2w', now(), now());
insert into ab_test_group (ab_test_id, bucket_id, value, weight, description, created_at, updated_at) VALUES (4, 6, 6, 15,   '15% at 8% 2w', now(), now());


