# ---!Ups


insert into requirement (id, type, value, mvel, created_at, updated_at) values
  (15, 'DOCUMENT', 'CHEQUE_LEAF', 'input.isDocProcessed("CHEQUE_LEAF")', '2017-09-10', now() );


insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (30015, 30, 15, '2017-09-10', now());
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (31015, 31, 15, '2017-09-10', now());
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (32015, 32, 15, '2017-09-10', now());
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (33015, 33, 15, '2017-09-10', now());
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (34015, 34, 15, '2017-09-10', now());
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (35015, 35, 15, '2017-09-10', now());
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (36015, 36, 15, '2017-09-10', now());
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (40015, 40, 15, '2017-09-10', now());
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (41015, 41, 15, '2017-09-10', now());
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values (42015, 42, 15, '2017-09-10', now());


# ---!Downs

delete from requirement where id = 15;
delete from loan_product_requirement where id in (30015,31015,32015,33015,34015,35015,36015,40015,41015,42015 );