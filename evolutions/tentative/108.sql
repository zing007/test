# ---!Ups

insert into ab_test (config_key, start_date, end_date, description, created_at, updated_at) VALUES ('show_cheque_leaf', now(), now() + interval 1 year,'Aadhaar QR Scan flow testing', now(), now());
insert into ab_test_config (property_key, property_value, created_at, updated_at) VALUES ('show_cheque_leaf', 0, now(), now());
insert into ab_test_group (ab_test_id, bucket_id, value, weight, description, created_at, updated_at) VALUES (2, 1, 0, 50,'Hide Cheque Leaf', now(), now());
insert into ab_test_group (ab_test_id, bucket_id, value, weight, description, created_at, updated_at) VALUES (2, 2, 1, 50,'Show Cheque Leaf', now(), now());

# ---!Downs

delete from ab_test_group  where ab_test_id in ( select id from ab_test
    where config_key = 'show_cheque_leaf' );
delete from ab_test_config where property_key = 'show_cheque_leaf';
delete from ab_test where config_key = 'show_cheque_leaf';

