
update loan_product set end_date = '2018-10-07' where name in ('3000_240_2WEEK_v2', '3000_300_2WEEK_v2',
        '3000_360_2WEEK_v2', '4000_320_2WEEK_v2', '4000_400_2WEEK_v2', '5000_400_2WEEK_v2', '5000_500_2WEEK_v2',
        '6000_360_2WEEK_v2', '6000_420_2WEEK_v2', '6000_480_2WEEK_v2', '6000_600_2WEEK_v2');

INSERT INTO `loan_product` VALUES (3081,3000,3,240,1,1,'MONTH','2018-10-06 00:00:01','2027-10-06 00:00:01',
    '2018-10-05 11:33:30','2018-10-05 11:33:30','3000_240_1MONTH_v2',3080,'loan_product_pricing_v2','9');
INSERT INTO `loan_product` VALUES (3101,3000,3,300,1,1,'MONTH','2018-10-06 00:00:01','2027-10-06 00:00:01',
    '2018-10-05 11:33:30','2018-10-05 11:33:30','3000_300_1MONTH_v2',3100,'loan_product_pricing_v2','8');
INSERT INTO `loan_product` VALUES (3121,3000,3,360,1,1,'MONTH','2018-10-06 00:00:01','2027-10-06 00:00:01',
    '2018-07-25 05:33:29','2018-07-25 05:33:29','3000_360_1MONTH_v2',3120,'loan_product_pricing_v2','8');
INSERT INTO `loan_product` VALUES (4081,4000,3,320,1,1,'MONTH','2018-10-06 00:00:01','2027-10-06 00:00:01',
    '2018-10-05 11:33:30','2018-10-05 11:33:30','4000_320_1MONTH_v2',4080,'loan_product_pricing_v2','9');
INSERT INTO `loan_product` VALUES (4101,4000,3,400,1,1,'MONTH','2018-10-06 00:00:01','2027-10-06 00:00:01',
    '2018-07-25 05:33:29','2018-07-25 05:33:29','4000_400_1MONTH_v2',4100,'loan_product_pricing_v2','8');
INSERT INTO `loan_product` VALUES (5081,5000,3,400,1,1,'MONTH','2018-10-06 00:00:01','2027-10-06 00:00:01',
    '2018-10-05 11:33:30','2018-10-05 11:33:30','5000_400_1MONTH_v2',5080,'loan_product_pricing_v2','9');
INSERT INTO `loan_product` VALUES (5101,5000,3,500,1,1,'MONTH','2018-10-06 00:00:01','2027-10-06 00:00:01',
    '2018-07-25 05:33:29','2018-07-25 05:33:29','5000_500_1MONTH_v2',5100,'loan_product_pricing_v2','8');
INSERT INTO `loan_product` VALUES (6061,6000,3,360,1,1,'MONTH','2018-10-06 00:00:01','2027-10-06 00:00:01',
    '2018-10-05 11:33:30','2018-10-05 11:33:30','6000_360_1MONTH_v2',6060,'loan_product_pricing_v2','9');
INSERT INTO `loan_product` VALUES (6071,6000,3,420,1,1,'MONTH','2018-10-06 00:00:01','2027-10-06 00:00:01',
    '2018-10-05 11:33:30','2018-10-05 11:33:30','6000_420_1MONTH_v2',6070,'loan_product_pricing_v2','8');
INSERT INTO `loan_product` VALUES (6081,6000,3,480,1,1,'MONTH','2018-10-06 00:00:01','2027-10-06 00:00:01',
    '2018-10-05 11:33:30','2018-10-05 11:33:30','6000_480_1MONTH_v2',6080,'loan_product_pricing_v2','9');
INSERT INTO `loan_product` VALUES (6101,6000,3,600,1,1,'MONTH','2018-10-06 00:00:01','2027-10-06 00:00:01',
    '2018-10-05 11:33:30','2018-10-05 11:33:30','6000_600_1MONTH_v2',6100,'loan_product_pricing_v2','8');


update loan_product_dependency set enabled =0 where  target in ('3000_240_2WEEK_v2', '3000_300_2WEEK_v2', '3000_360_2WEEK_v2',
    '4000_320_2WEEK_v2', '4000_400_2WEEK_v2', '5000_400_2WEEK_v2', '5000_500_2WEEK_v2',
     '6000_360_2WEEK_v2', '6000_420_2WEEK_v2', '6000_480_2WEEK_v2', '6000_600_2WEEK_v2');
update  loan_product_dependency lpd, loan_product lp set lpd.enabled=0 where lpd.target = lp.name
        and lp.end_date <= now() and lpd.enabled=1;


     INSERT INTO `loan_product_dependency` VALUES (1488,'rb_default_self_3000_300_2WEEK_v2_6_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','3000_300_2WEEK_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','3000_300_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1489,'rb_default_self_3000_300_2WEEK_v2_6_repeat_unlocked','r2_riskband','RISK_BAND','6',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','3000_300_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1490,'rb_default_self_3000_360_2WEEK_v2_6_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','3000_360_2WEEK_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','3000_360_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1491,'rb_default_self_3000_360_2WEEK_v2_6_repeat_unlocked','r2_riskband','RISK_BAND','6',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','3000_360_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1492,'rb_default_self_4000_320_2WEEK_v2_5_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','4000_320_2WEEK_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','4000_320_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1493,'rb_default_self_4000_320_2WEEK_v2_5_repeat_unlocked','r2_riskband','RISK_BAND','5',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','4000_320_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1494,'rb_default_self_4000_400_2WEEK_v2_5_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','4000_400_2WEEK_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','4000_400_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1495,'rb_default_self_4000_400_2WEEK_v2_5_repeat_unlocked','r2_riskband','RISK_BAND','5',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','4000_400_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1496,'rb_default_self_5000_400_2WEEK_v2_4_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','5000_400_2WEEK_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','5000_400_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1497,'rb_default_self_5000_400_2WEEK_v2_4_repeat_unlocked','r2_riskband','RISK_BAND','4',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','5000_400_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1498,'rb_default_self_5000_400_2WEEK_v2_6_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','5000_400_2WEEK_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','5000_400_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1499,'rb_default_self_5000_400_2WEEK_v2_6_repeat_unlocked','r2_riskband','RISK_BAND','6',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','5000_400_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1500,'rb_default_self_5000_500_2WEEK_v2_4_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','5000_500_2WEEK_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','5000_500_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1501,'rb_default_self_5000_500_2WEEK_v2_4_repeat_unlocked','r2_riskband','RISK_BAND','4',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','5000_500_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1502,'rb_default_self_5000_500_2WEEK_v2_6_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','5000_500_2WEEK_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','5000_500_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1503,'rb_default_self_5000_500_2WEEK_v2_6_repeat_unlocked','r2_riskband','RISK_BAND','6',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','5000_500_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1504,'rb_default_self_6000_360_2WEEK_v2_5_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','6000_360_2WEEK_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','6000_360_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1505,'rb_default_self_6000_360_2WEEK_v2_5_repeat_unlocked','r2_riskband','RISK_BAND','5',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','6000_360_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1506,'rb_default_self_6000_420_2WEEK_v2_5_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','6000_420_2WEEK_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','6000_420_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1507,'rb_default_self_6000_420_2WEEK_v2_5_repeat_unlocked','r2_riskband','RISK_BAND','5',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','6000_420_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1508,'rb_default_self_6000_480_2WEEK_v2_1_2_3_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','6000_480_2WEEK_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','6000_480_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1509,'rb_default_self_6000_480_2WEEK_v2_1_2_3_repeat_unlocked','r2_riskband','RISK_BAND','1,2,3',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','6000_480_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1510,'rb_default_self_6000_600_2WEEK_v2_1_2_3_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','6000_600_2WEEK_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','6000_600_1MONTH_v2',1,'  ');
     INSERT INTO `loan_product_dependency` VALUES (1511,'rb_default_self_6000_600_2WEEK_v2_1_2_3_repeat_unlocked','r2_riskband','RISK_BAND','1,2,3',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','6000_600_1MONTH_v2',1,'  ');


INSERT INTO `loan_product_dependency` VALUES (1293,'rb_default_0601_3000_360_1MONTH_v2','r1','LOAN','1000','2',NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','3000_360_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1294,'rb_default_0601_3000_360_1MONTH_v2','r2','RISK_BAND','6',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','3000_360_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1295,'rb_default_0602_5000_500_1MONTH_v2','r1','LOAN','3000','2',NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','5000_500_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1296,'rb_default_0602_5000_500_1MONTH_v2','r2','RISK_BAND','6',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','5000_500_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1303,'rb_default_0501_4000_400_1MONTH_v2','r1','LOAN','2000','2',NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','4000_400_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1304,'rb_default_0501_4000_400_1MONTH_v2','r2','RISK_BAND','5',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','4000_400_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1305,'rb_default_0502_6000_420_1MONTH_v2','r1','LOAN','4000','2',NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','6000_420_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1306,'rb_default_0502_6000_420_1MONTH_v2','r2','RISK_BAND','5',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','6000_420_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1311,'rb_default_0400_3000_300_1MONTH_v2','r1','MVEL_EXPRESSION','1 == 1','1',NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','3000_300_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1312,'rb_default_0400_3000_300_1MONTH_v2','r2','RISK_BAND','4',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','3000_300_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1313,'rb_default_0401_5000_500_1MONTH_v2','r1','LOAN','3000','2',NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','5000_500_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1314,'rb_default_0401_5000_500_1MONTH_v2','r2','RISK_BAND','4',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','5000_500_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1321,'rb_default_0300_4000_400_1MONTH_v2','r1','MVEL_EXPRESSION','1 == 1','1',NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','4000_400_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1322,'rb_default_0300_4000_400_1MONTH_v2','r2','RISK_BAND','0,1,2,3',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','4000_400_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1323,'rb_default_0301_6000_600_1MONTH_v2','r1','LOAN','4000','2',NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','6000_600_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1324,'rb_default_0301_6000_600_1MONTH_v2','r2','RISK_BAND','0,1,2,3',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','6000_600_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1331,'rb_default_0100_5000_500_1MONTH_v2','r1','MVEL_EXPRESSION','1 == 1','1',NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','5000_500_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1332,'rb_default_0100_5000_500_1MONTH_v2','r2','RISK_BAND','1,2',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','5000_500_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1343,'rb_default_0601_3000_300_1MONTH_v2','r1','LOAN','1000','2',NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','3000_300_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1344,'rb_default_0601_3000_300_1MONTH_v2','r2','RISK_BAND','6',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','3000_300_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1345,'rb_default_0602_5000_400_1MONTH_v2','r1','LOAN','3000','2',NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','5000_400_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1346,'rb_default_0602_5000_400_1MONTH_v2','r2','RISK_BAND','6',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','5000_400_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1353,'rb_default_0501_4000_320_1MONTH_v2','r1','LOAN','2000','2',NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','4000_320_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1354,'rb_default_0501_4000_320_1MONTH_v2','r2','RISK_BAND','5',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','4000_320_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1355,'rb_default_0502_6000_360_1MONTH_v2','r1','LOAN','4000','2',NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','6000_360_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1356,'rb_default_0502_6000_360_1MONTH_v2','r2','RISK_BAND','5',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','6000_360_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1361,'rb_default_0400_3000_240_1MONTH_v2','r1','MVEL_EXPRESSION','1 == 1','1',NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','3000_240_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1362,'rb_default_0400_3000_240_1MONTH_v2','r2','RISK_BAND','4',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','3000_240_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1363,'rb_default_0401_5000_400_1MONTH_v2','r1','LOAN','3000','2',NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','5000_400_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1364,'rb_default_0401_5000_400_1MONTH_v2','r2','RISK_BAND','4',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','5000_400_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1371,'rb_default_0300_4000_320_1MONTH_v2','r1','MVEL_EXPRESSION','1 == 1','1',NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','4000_320_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1372,'rb_default_0300_4000_320_1MONTH_v2','r2','RISK_BAND','0,1,2,3',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','4000_320_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1373,'rb_default_0301_6000_480_1MONTH_v2','r1','LOAN','4000','2',NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','6000_480_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1374,'rb_default_0301_6000_480_1MONTH_v2','r2','RISK_BAND','0,1,2,3',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','6000_480_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1381,'rb_default_0100_5000_400_1MONTH_v2','r1','MVEL_EXPRESSION','1 == 1','1',NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','5000_400_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1382,'rb_default_0100_5000_400_1MONTH_v2','r2','RISK_BAND','1,2',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','5000_400_1MONTH_v2',1,'  ');

INSERT INTO `loan_product_dependency` VALUES (1388,'rb_default_self_3000_300_1MONTH_v2_6_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','3000_300_1MONTH_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','3000_300_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1389,'rb_default_self_3000_300_1MONTH_v2_6_repeat_unlocked','r2_riskband','RISK_BAND','6',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','3000_300_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1390,'rb_default_self_3000_360_1MONTH_v2_6_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','3000_360_1MONTH_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','3000_360_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1391,'rb_default_self_3000_360_1MONTH_v2_6_repeat_unlocked','r2_riskband','RISK_BAND','6',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','3000_360_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1392,'rb_default_self_4000_320_1MONTH_v2_5_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','4000_320_1MONTH_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','4000_320_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1393,'rb_default_self_4000_320_1MONTH_v2_5_repeat_unlocked','r2_riskband','RISK_BAND','5',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','4000_320_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1394,'rb_default_self_4000_400_1MONTH_v2_5_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','4000_400_1MONTH_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','4000_400_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1395,'rb_default_self_4000_400_1MONTH_v2_5_repeat_unlocked','r2_riskband','RISK_BAND','5',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','4000_400_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1396,'rb_default_self_5000_400_1MONTH_v2_4_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','5000_400_1MONTH_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','5000_400_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1397,'rb_default_self_5000_400_1MONTH_v2_4_repeat_unlocked','r2_riskband','RISK_BAND','4',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','5000_400_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1398,'rb_default_self_5000_400_1MONTH_v2_6_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','5000_400_1MONTH_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','5000_400_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1399,'rb_default_self_5000_400_1MONTH_v2_6_repeat_unlocked','r2_riskband','RISK_BAND','6',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','5000_400_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1400,'rb_default_self_5000_500_1MONTH_v2_4_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','5000_500_1MONTH_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','5000_500_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1401,'rb_default_self_5000_500_1MONTH_v2_4_repeat_unlocked','r2_riskband','RISK_BAND','4',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','5000_500_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1402,'rb_default_self_5000_500_1MONTH_v2_6_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','5000_500_1MONTH_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','5000_500_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1403,'rb_default_self_5000_500_1MONTH_v2_6_repeat_unlocked','r2_riskband','RISK_BAND','6',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','5000_500_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1404,'rb_default_self_6000_360_1MONTH_v2_5_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','6000_360_1MONTH_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','6000_360_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1405,'rb_default_self_6000_360_1MONTH_v2_5_repeat_unlocked','r2_riskband','RISK_BAND','5',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','6000_360_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1406,'rb_default_self_6000_420_1MONTH_v2_5_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','6000_420_1MONTH_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','6000_420_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1407,'rb_default_self_6000_420_1MONTH_v2_5_repeat_unlocked','r2_riskband','RISK_BAND','5',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','6000_420_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1408,'rb_default_self_6000_480_1MONTH_v2_1_2_3_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','6000_480_1MONTH_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','6000_480_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1409,'rb_default_self_6000_480_1MONTH_v2_1_2_3_repeat_unlocked','r2_riskband','RISK_BAND','1,2,3',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','6000_480_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1410,'rb_default_self_6000_600_1MONTH_v2_1_2_3_repeat_unlocked','r1_lp','LOAN_PRODUCT_NAME','6000_600_1MONTH_v2','1',NULL,NULL,'false','2018-10-05 11:33:30','2018-10-05 11:33:30','6000_600_1MONTH_v2',1,'  ');
INSERT INTO `loan_product_dependency` VALUES (1411,'rb_default_self_6000_600_1MONTH_v2_1_2_3_repeat_unlocked','r2_riskband','RISK_BAND','1,2,3',NULL,NULL,NULL,NULL,'2018-10-05 11:33:30','2018-10-05 11:33:30','6000_600_1MONTH_v2',1,'  ');
