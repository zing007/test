# ---!Ups
insert into requirement (id, type, value, mvel, created_at, updated_at) values
  (10, 'DOCUMENT', 'PAN', 'input.isDocProcessed("PAN")', '2017-07-01', now() );

insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (7010, 7, 10, '2017-07-31', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (8010, 8, 10, '2017-07-31', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (9010, 9, 10, '2017-07-31', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (11010, 11, 10, '2017-07-31', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (15010, 15, 10, '2017-07-31', now() );

insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (20010, 20, 10, '2017-07-31', now() );

insert into loan_product_dependency (id, rule, sub_rule, type, param1, param2, param3, param4, param5,
        created_at, updated_at, target, enabled, description) values
        (20, 'default2', '', 'LOAN', '3000',
        "1", "1", "MONTH", NULL, now(), now(), '3000_150_1MONTH', 1, '  ');

# ---!Downs
delete from requirement where id = 10;

delete from loan_product_requirement where id in (7010, 8010, 9010, 11010, 15010, 20010);

