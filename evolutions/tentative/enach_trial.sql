
insert into requirement (type, value, mvel, created_at, updated_at) values
  ('DOCUMENT', 'ENACH', 'input.isDocProcessed("ENACH")', '2017-07-01', now() );

insert into ab_test_config values (6, 'enach_trial',  "0", now(), now());

insert into ab_test values (6,  'enach_trial', '2018-08-30 01:00:00',  '2028-08-31', 'eNach trial', now(), now());

insert into ab_test_group values (60000, 6,0, "0", 100, 'do not show eNach', now(), now() );
insert into ab_test_group values (60001, 6,1, "1", 0,   'show eNach', now(), now() );

insert into user_ab_group(user_id, test_id, bucket_id, created_at, updated_at)
    (select ul.user_id, 6, 1, '2018-08-30 01:00:00', '2018-08-30 01:00:00'
            from user_loan_product ulp, user_loan_product_requirement ulpr,
                (select user_id, max(updated_at) as mxuat   from user_loan where status='COMPLETED' group by user_id) ul,
                loan_product lp , user_risk_band b,
                (select user_id, min(score) as mis from user_intent_scores
                    where status = 'SUCCESS'  and score > 50 group by user_id ) as uis
    where ul.user_id   = ulp.user_id  and ulp.id = ulpr.user_loan_product_id
        and ul.user_id = uis.user_id and uis.mis > 70  and ulpr.requirement_id in (2,3,4,5)
        and ulp.loan_product_id = lp.id and lp.end_date > '2018-12-01'  and mxuat > '2018-01-01'
        and ul.user_id = b.user_id and b.risk_band in (2,3,5,6)
    group by ul.user_id  );
