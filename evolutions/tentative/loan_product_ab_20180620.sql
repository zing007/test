
insert into ab_test_group (ab_test_id, bucket_id, value, weight, description, created_at, updated_at) VALUES (4, 7, 7, 40,   '40% at 10% 2w', now(), now());
update ab_test_group set weight = 0, description='No Change' where ab_test_id = 4 and bucket_id = 1 ;
update ab_test_group set weight = 0, description='0% at  8% 1m' where ab_test_id = 4 and bucket_id = 2 ;
update ab_test_group set weight = 0, description='0% at 10% 1m' where ab_test_id = 4 and bucket_id = 3 ;
update ab_test_group set weight = 0, description='0% at 12% 1m' where ab_test_id = 4 and bucket_id = 4 ;
update ab_test_group set weight = 30, description='30% at 5% 2w' where ab_test_id = 4 and bucket_id = 5 ;
update ab_test_group set weight = 40, description='30% at 8% 2w' where ab_test_id = 4 and bucket_id = 6 ;


update loan_product set group_value = 6 where name = '1000_80_2WEEK'  ;
update loan_product set group_value = 7 where name = '1000_99_2WEEK'  ;
update loan_product set group_value = NULL where name = '1000_120_2WEEK' ;

update loan_product set group_value = 6 where name = '2000_160_2WEEK'  ;
update loan_product set group_value = 7 where name = '2000_200_2WEEK'  ;
update loan_product set group_value = NULL where name = '2000_240_2WEEK' ;


insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (3007, 3000, 3, 300, 1, 2,
     'WEEK', '2018-04-22', '2027-06-20', now(), now(), '3000_300_2WEEK', -1, 'loan_product_pricing', 7);
update loan_product l1, loan_product l2 set l1.base_lp_id = l2.id where l2.name='3000_150_1MONTH' and l1.name
     in ('3000_300_2WEEK');

insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (4007, 4000, 3, 400, 1, 2,
     'WEEK', '2018-04-22', '2027-06-20', now(), now(), '4000_400_2WEEK', -1, 'loan_product_pricing', 7);
update loan_product l1, loan_product l2 set l1.base_lp_id = l2.id where l2.name='4000_200_1MONTH' and l1.name
     in ('4000_400_2WEEK');


insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (5007, 5000, 3, 500, 1, 2,
     'WEEK', '2018-04-22', '2027-06-20', now(), now(), '5000_500_2WEEK', -1, 'loan_product_pricing', 7);
update loan_product l1, loan_product l2 set l1.base_lp_id = l2.id where l2.name='5000_200_1MONTH' and l1.name
     in ('5000_500_2WEEK');


insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (6007, 6000, 3, 600, 1, 2,
     'WEEK', '2018-04-22', '2027-06-20', now(), now(), '6000_600_2WEEK', -1, 'loan_product_pricing', 7);
update loan_product l1, loan_product l2 set l1.base_lp_id = l2.id where l2.name='6000_300_1MONTH' and l1.name
     in ('6000_600_2WEEK');



insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (10006, 10000, 3, 350, 1, 2,
     'MONTH', '2018-04-22', '2027-06-20', now(), now(), '10000_350_2MONTH', -1, 'loan_product_pricing', 6);
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (10007, 10000, 3, 500, 1, 2,
     'MONTH', '2018-04-22', '2027-06-20', now(), now(), '10000_500_2MONTH', -1, 'loan_product_pricing', 7);
update loan_product l1, loan_product l2 set l1.base_lp_id = l2.id where l2.name='10000_200_2MONTH' and l1.name
     in ('10000_350_2MONTH', '10000_500_2MONTH');
update loan_product set group_key='loan_product_pricing', group_value = 5 where name = '10000_200_2MONTH' ;



insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (15006, 15000, 3, 400, 1, 3,
     'MONTH', '2018-04-22', '2027-06-20', now(), now(), '15000_400_3MONTH', -1, 'loan_product_pricing', 6);
update loan_product set group_key='loan_product_pricing',  group_value = 7, end_date='2027-06-20', base_lp_id=-1
        where name = '15000_500_3MONTH' ;
update loan_product l1, loan_product l2 set l1.base_lp_id = l2.id where l2.name='15000_300_3MONTH' and l1.name
     in ('15000_400_3MONTH', '15000_500_3MONTH');
update loan_product set group_key='loan_product_pricing',  group_value = 5 where name = '15000_300_3MONTH' ;


