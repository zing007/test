# ---!Ups

insert into ab_test (config_key, start_date, end_date, description, created_at, updated_at) VALUES ('aadhaar_qr_scan', now(), now() + interval 1 year,'Aadhaar QR Scan flow testing', now(), now());
insert into ab_test_config (property_key, property_value, created_at, updated_at) VALUES ('aadhaar_qr_scan', 0, now(), now());
insert into ab_test_group (ab_test_id, bucket_id, value, weight, description, created_at, updated_at) VALUES (1, 1, 0, 80,'Aadhaar QR Scan off', now(), now());
insert into ab_test_group (ab_test_id, bucket_id, value, weight, description, created_at, updated_at) VALUES (1, 2, 1, 20,'Aadhaar QR Scan on', now(), now());

# ---!Downs

delete from ab_test_group  where ab_test_id in ( select id from ab_test
    where config_key = 'aadhaar_qr_scan' );
delete from ab_test_config where property_key = 'aadhaar_qr_scan';
delete from ab_test where config_key = 'aadhaar_qr_scan';

