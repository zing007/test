# ---!Ups

insert into requirement (id, type, value, mvel, created_at, updated_at) values
  (9, 'DOCUMENT', 'NACH', 'input.isDocProcessed("NACH")', '2017-07-01', now() );
insert into requirement (id, type, value, mvel, created_at, updated_at) values
  (11, 'DOCUMENT', 'BANK_STMT', 'input.isDocProcessed("BANK_STMT")', '2017-07-01', now() );
insert into requirement (id, type, value, mvel, created_at, updated_at) values
  (12, 'DOCUMENT', 'SALARY_SLIP__1M', '!input.isSalaried() || input.isDocProcessed("SALARY_SLIP__1M")', '2017-07-01', now() );
insert into requirement (id, type, value, mvel, created_at, updated_at) values
  (13, 'DOCUMENT', 'SALARY_SLIP__2M', '!input.isSalaried() || input.isDocProcessed("SALARY_SLIP__2M")', '2017-07-01', now() );
insert into requirement (id, type, value, mvel, created_at, updated_at) values
  (14, 'DOCUMENT', 'SALARY_SLIP__3M', '!input.isSalaried() || input.isDocProcessed("SALARY_SLIP__3M")', '2017-07-01', now() );


insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name) values (30, 4000, 3, 200, 1, 1, 'MONTH', '2017-07-05',
       '2027-06-20', now(), now(), 'VLP_4000_200_1MONTH');

insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name) values (31, 5000, 3, 200, 1, 1, 'MONTH', '2017-07-05',
       '2027-06-20', now(), now(), 'VLP_5000_200_1MONTH');

insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name) values (32, 6000, 3, 300, 1, 1, 'MONTH', '2017-07-05',
       '2027-06-20', now(), now(), 'VLP_6000_300_1MONTH');

insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name) values (33, 7000, 3, 300, 1, 1, 'MONTH', '2017-07-05',
       '2027-06-20', now(), now(), 'VLP_7000_200_1MONTH');

insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name) values (34, 8000, 3, 300, 1, 1, 'MONTH', '2017-07-05',
       '2027-06-20', now(), now(), 'VLP_8000_300_1MONTH');

insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name) values (35, 9000, 3, 300, 1, 1, 'MONTH', '2017-07-05',
       '2027-06-20', now(), now(), 'VLP_9000_200_1MONTH');

insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
       end_date, created_at, updated_at, name) values (36, 10000, 3, 300, 1, 1, 'MONTH', '2017-07-05',
       '2017-08-08 06:05:00', now(), now(), 'VLP_10000_300_1MONTH');

insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (30010, 30, 10, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (30011, 30, 11, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (30012, 30, 12, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (30013, 30, 13, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (30014, 30, 14, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (31010, 31, 10, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (31011, 31, 11, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (31012, 31, 12, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (31013, 31, 13, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (31014, 31, 14, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (32010, 32, 10, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (32011, 32, 11, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (32012, 32, 12, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (32013, 32, 13, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (32014, 32, 14, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (33010, 33, 10, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (33011, 33, 11, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (33012, 33, 12, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (33013, 33, 13, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (33014, 33, 14, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (34010, 34, 10, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (34011, 34, 11, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (34012, 34, 12, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (34013, 34, 13, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (34014, 34, 14, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (35010, 35, 10, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (35011, 35, 11, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (35012, 35, 12, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (35013, 35, 13, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (35014, 35, 14, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (36010, 36, 10, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (36011, 36, 11, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (36012, 36, 12, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (36013, 36, 13, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (36014, 36, 14, '2017-07-05', now() );

insert into loan_product_dependency (id, rule, sub_rule, type, param1, param2, param3, param4, param5,
        created_at, updated_at, target, enabled, description) values
        (30, 'vlp_4000', '', 'MVEL_EXPRESSION', 'input.showVlp(4000)',
        NULL, NULL, NULL, NULL, now(), now(), 'VLP_4000_200_1MONTH', 1, 'virtual LP 4000 ');

insert into loan_product_dependency (id, rule, sub_rule, type, param1, param2, param3, param4, param5,
        created_at, updated_at, target, enabled, description) values
        (31, 'vlp_5000', '', 'MVEL_EXPRESSION', 'input.showVlp(5000)',
        NULL, NULL, NULL, NULL, now(), now(), 'VLP_5000_200_1MONTH', 1, 'virtual LP 5000 ');

insert into loan_product_dependency (id, rule, sub_rule, type, param1, param2, param3, param4, param5,
        created_at, updated_at, target, enabled, description) values
        (32, 'vlps_6000', '', 'MVEL_EXPRESSION', 'input.showVlp(6000)',
        NULL, NULL, NULL, NULL, now(), now(), 'VLP_6000_300_1MONTH', 1, 'virtual LP 6000');

insert into loan_product_dependency (id, rule, sub_rule, type, param1, param2, param3, param4, param5,
        created_at, updated_at, target, enabled, description) values
        (33, 'vlp_7000', '', 'MVEL_EXPRESSION', 'input.showVlp(7000)',
        NULL, NULL, NULL, NULL, now(), now(), 'VLP_7000_300_1MONTH', 1, 'virtual LP 7000 ');


insert into loan_product_dependency (id, rule, sub_rule, type, param1, param2, param3, param4, param5,
        created_at, updated_at, target, enabled, description) values
        (34, 'vlps_8000', '', 'MVEL_EXPRESSION', 'input.showVlp(8000)',
        NULL, NULL, NULL, NULL, now(), now(), 'VLP_8000_300_1MONTH', 1, 'virtual LP 8000');

insert into loan_product_dependency (id, rule, sub_rule, type, param1, param2, param3, param4, param5,
        created_at, updated_at, target, enabled, description) values
        (35, 'vlp_9000', '', 'MVEL_EXPRESSION', 'input.showVlp(9000)',
        NULL, NULL, NULL, NULL, now(), now(), 'VLP_9000_300_1MONTH', 1, 'virtual LP 9000 ');

insert into loan_product_dependency (id, rule, sub_rule, type, param1, param2, param3, param4, param5,
        created_at, updated_at, target, enabled, description) values
        (36, 'vlps_10000', '', 'MVEL_EXPRESSION', 'input.showVlp(10000)',
        NULL, NULL, NULL, NULL, now(), now(), 'VLP_10000_300_1MONTH', 1, 'virtual LP 10000');



insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (7011, 7, 11, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (7012, 7, 12, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (7013, 7, 13, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (7014, 7, 14, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (8011, 8, 11, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (8012, 8, 12, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (8013, 8, 13, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (8014, 8, 14, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (9011, 9, 11, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (9012, 9, 12, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (9013, 9, 13, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (9014, 9, 14, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (11011, 11, 11, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (11012, 11, 12, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (11013, 11, 13, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (11014, 11, 14, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (15011, 15, 11, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (15012, 15, 12, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (15013, 15, 13, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (15014, 15, 14, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (20011, 20, 11, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (20012, 20, 12, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (20013, 20, 13, '2017-07-05', now() );
insert into loan_product_requirement (id, loan_product_id, requirement_id, created_at, updated_at) values
                            (20014, 20, 14, '2017-07-05', now() );


insert into features (name, source, identity_engine, intent_engine, min_threshold, max_threshold, is_asc,
     created_at, updated_at, capacity_engine, waiting_engine, description, redash_query_id, redash_params) values
     ('bs_avgCashFlow_salary_1m', 'bank-stmt', 0, 1, 0, 10000000, 1, '2017-07-05', now(), 1, 0, NULL, 0, NULL),
     ('bs_avgCashFlow_balance_1m', 'bank-stmt', 0, 1, 0, 10000000, 1, '2017-07-05', now(), 1, 0, NULL, 0, NULL),
     ('bs_avgCashFlow_inflow_1m', 'bank-stmt', 0, 1, 0, 10000000, 1, '2017-07-05', now(), 1, 0, NULL, 0, NULL),
     ('bs_avgCashFlow_outflow_1m', 'bank-stmt', 0, 1, 0, 10000000, 1, '2017-07-05', now(), 1, 0, NULL, 0, NULL),
     ('bs_avgCashFlow_salary_2m', 'bank-stmt', 0, 1, 0, 10000000, 1, '2017-07-05', now(), 1, 0, NULL, 0, NULL),
     ('bs_avgCashFlow_balance_2m', 'bank-stmt', 0, 1, 0, 10000000, 1, '2017-07-05', now(), 1, 0, NULL, 0, NULL),
     ('bs_avgCashFlow_inflow_2m', 'bank-stmt', 0, 1, 0, 10000000, 1, '2017-07-05', now(), 1, 0, NULL, 0, NULL),
     ('bs_avgCashFlow_outflow_2m', 'bank-stmt', 0, 1, 0, 10000000, 1, '2017-07-05', now(), 1, 0, NULL, 0, NULL),
     ('bs_avgCashFlow_salary_3m', 'bank-stmt', 0, 1, 0, 10000000, 1, '2017-07-05', now(), 1, 0, NULL, 0, NULL),
     ('bs_avgCashFlow_balance_3m', 'bank-stmt', 0, 1, 0, 10000000, 1, '2017-07-05', now(), 1, 0, NULL, 0, NULL),
     ('bs_avgCashFlow_inflow_3m', 'bank-stmt', 0, 1, 0, 10000000, 1, '2017-07-05', now(), 1, 0, NULL, 0, NULL),
     ('bs_avgCashFlow_outflow_3m', 'bank-stmt', 0, 1, 0, 10000000, 1, '2017-07-05', now(), 1, 0, NULL, 0, NULL);

# ---!Downs
delete from loan_product_requirement where id in (30010, 30011, 30012, 30013, 30014, 31010, 31011, 31012, 31013, 31014,
               32010, 32011, 32012, 32013, 32014, 33010, 33011, 33012, 33013, 33014, 34010, 34011, 34012, 34013, 34014,
               35010, 35011, 35012, 35013, 35014, 36010, 36011, 36012, 36013, 36014);

delete from loan_product_requirement where id in (7010, 7011, 7012, 7013, 7014, 8010, 8011, 8012, 8013, 8014,
                                             9010, 9011, 9012, 9013, 9014, 11010, 11011, 11012, 11013, 11014,
                                             15010, 15011, 15012, 15013, 15014, 20010, 20011, 20012, 20013, 20014);

delete from requirement where id >= 9 and id <= 14;

delete from loan_product_dependency where id >= 30 and id <= 36;

delete from loan_product where  id >= 30 and id <= 36;

delete from features where source = 'bank-stmt' and name in ('bs_avgCashFlow_salary_1m', 'bs_avgCashFlow_balance_1m',
   'bs_avgCashFlow_inflow_1m', 'bs_avgCashFlow_outflow_1m', 'bs_avgCashFlow_salary_2m', 'bs_avgCashFlow_balance_2m',
   'bs_avgCashFlow_inflow_2m', 'bs_avgCashFlow_outflow_2m', 'bs_avgCashFlow_salary_3m', 'bs_avgCashFlow_balance_3m',
   'bs_avgCashFlow_inflow_3m', 'bs_avgCashFlow_outflow_3m');

