
--  add risk-band dependency for old loan-products

update  loan_product set end_date = '2018-07-21' where not(name like 'WAITVLP%') or (end_date  > now());
update loan_product_dependency set sub_rule = 'default_sr' where sub_rule is NULL or length(sub_rule) < 1;


drop table if exists loan_product_dependency_tmp;
CREATE TABLE `loan_product_dependency_tmp` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rule` varchar(255) DEFAULT NULL,
  `sub_rule` varchar(255) DEFAULT NULL,
  `type` varchar(17) DEFAULT NULL,
  `param1` varchar(255) DEFAULT NULL,
  `param2` varchar(255) DEFAULT NULL,
  `param3` varchar(255) DEFAULT NULL,
  `param4` varchar(255) DEFAULT NULL,
  `param5` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `target` varchar(255) NOT NULL,
  `enabled` tinyint(1) DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

truncate table loan_product_dependency_tmp;
delete from loan_product_dependency where id > 1000;
insert into loan_product_dependency_tmp(id, rule, sub_rule, type, param1, param2, param3, param4, param5,created_at, updated_at,
        target, enabled, description)  (select 1000+id , rule , 'r2_fksfl', 'RISK_BAND', '0', NULL, NULL, NULL,
        NULL, now(), now(), target , 1, '  ' from loan_product_dependency
        group by target, rule);
insert into loan_product_dependency (select * from loan_product_dependency_tmp);
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at,
    updated_at, target, enabled, description) values ('show_wait_on_3000__rb_below_0', 'r2b0', 'RISK_BAND',
    '-1', NULL, NULL, NULL, NULL, now(), now(), '3000_360_2WEEK_v2', 1, '  ');



-- add new WAIT_VLP as existings ones are expired
delete from waiting_criteria where id > 9;
insert into waiting_criteria (rule, sub_rule, type, param1, param2, param3, param4, param5,created_at, updated_at,
        target, enabled, description)  values ('risk_band_is_below_0' , 'r2_fksfl', 'RISK_BAND', '-1', NULL, NULL, NULL,
        NULL, now(), now(), '84' , 1, 'risk band below 0 - wait for 84 days');

insert into waiting_criteria (rule, sub_rule, type, param1, param2, param3, param4, param5,created_at, updated_at,
        target, enabled, description)  values ('high_risk_user_wait' , 'r2_fksfl', 'RISK_HEURISTICS', NULL, NULL, NULL, NULL,
        NULL, now(), now(), '84' , 1, 'highly risky user - wait for 84 days');


-- insert KYC requirement on LP
insert into requirement values (16, 'DOCUMENT', 'AADHAAR_FRONT', 'input.isDocProcessed("AADHAAR_FRONT")', '2018-07-20',  '2018-07-20');
insert into requirement values (18, 'DOCUMENT', 'AADHAAR_BACK' ,  'input.isDocProcessed("AADHAAR_BACK")', '2018-07-20',  '2018-07-20');
insert into requirement values (19, 'DOCUMENT', 'SIGNATURE' ,  'input.isDocProcessed("SIGNATURE")', '2018-07-20',  '2018-07-20');
insert into requirement values (20, 'DOCUMENT', 'USER_PHOTO' ,  'input.isDocProcessed("USER_PHOTO")', '2018-07-20',  '2018-07-20');

-- add new VLP of 6000 and 10000

insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (6051, 6000, 3, 300,
     1, 1, 'MONTH', '2018-07-20', '2027-07-20', now(), now(), 'VLP_6000_300_1MONTH_v2', 6051, 'loan_product_pricing_v2', -1);
insert into loan_product(id, amount, interest_rate, other_charges, emi_count, duration, duration_unit, start_date,
     end_date, created_at, updated_at, name, base_lp_id, group_key, group_value ) values (10051, 10000, 3, 500,
     1, 1, 'MONTH', '2018-07-20', '2027-07-20', now(), now(), 'VLP_10000_500_1MONTH_v2', 10051, 'loan_product_pricing_v2', -1);


insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target,
    enabled, description) values ('rb_default_VLP_6000_300_1MONTH_v2', 'r1', 'MVEL_EXPRESSION', 'input.showVlp(6000)', NULL, NULL, NULL, NULL, now(), now(), 'VLP_6000_300_1MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target,
    enabled, description) values ('rb_default_VLP_10000_500_1MONTH_v2', 'r1', 'LOAN', '4000', '1', NULL, NULL, NULL, now(), now(), 'VLP_10000_500_1MONTH_v2', 1, '  ');
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at, updated_at, target,
    enabled, description) values ('rb_default_VLP_10000_500_1MONTH_v2', 'r2', 'MVEL_EXPRESSION', 'input.showVlp(10000)', NULl, NULL, NULL, NULL, now(), now(), 'VLP_10000_500_1MONTH_v2', 1, '  ');

insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (6051, 16, '2018-07-20', now());
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (10051, 16, '2018-07-20', now());
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (6051, 18, '2018-07-20', now());
insert into loan_product_requirement(loan_product_id, requirement_id, created_at, updated_at) values (10051, 18, '2018-07-20', now());


-- disable previous AB test on loan_product_pricing and ignore-docs
update ab_test  set end_date = now() where config_key in ('loan_product_pricing', 'ignore_docs_for_financial_capacity');

insert into ab_test_config (property_key, property_value, created_at, updated_at) VALUES ('risk_band_pricing', "8", now(), now());
insert into ab_test (id, config_key, start_date, end_date, description, created_at, updated_at) VALUES (5, 'risk_band_pricing', now(), now() + interval 2 year,'Risk Bands', now(), now());
insert into ab_test_group (ab_test_id, bucket_id, value, weight, description, created_at, updated_at) VALUES (5, 1, 'high', 50, '50% - Higher Fee', now(), now());
insert into ab_test_group (ab_test_id, bucket_id, value, weight, description, created_at, updated_at) VALUES (5, 2, 'low', 50, '50% - Medium Fee', now(), now());






update loan_product_dependency set param1='0,1,2,3' where `type`='RISK_BAND' and id in (1122, 1124, 1126, 1172, 1174, 1176);
update loan_product_dependency set `type`='LOAN', param1='4000' where  id in (1133, 1183);
update loan_product_dependency set `type`='LOAN', param1='6000' where  id in (1135);
delete from loan_product_dependency where id=1089;
insert into loan_product_dependency (rule, sub_rule, type, param1, param2, param3, param4, param5, created_at,
    updated_at, target, enabled, description) values ('show_wait_on_10000__rb_below_0', 'r2b0', 'RISK_BAND',
    '-1', NULL, NULL, NULL, NULL, now(), now(), '10000_400_2MONTH_v2', 1, '  ');
delete from loan_product_dependency where rule='show_wait_on_3000__rb_below_0';